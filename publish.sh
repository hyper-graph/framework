set -e
yarn version:bump $1
yarn version:apply
yarn publish
echo "Success" && jq .version package.json
