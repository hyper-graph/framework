export type Identifiable<IdType = string> = {
    id: IdType;
};
