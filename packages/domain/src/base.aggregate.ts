import { BaseEntity } from './base.entity';
import type { Identifiable } from './identifiable';

const AGGREGATE_SYMBOL = Symbol('AGGREGATE_SYMBOL');

export abstract class BaseAggregate<IdType = string> extends BaseEntity<IdType> {
	protected readonly [AGGREGATE_SYMBOL]: IdType;

	protected constructor(params: Identifiable<IdType>) {
		super(params);

		Object.defineProperty(this, AGGREGATE_SYMBOL, {
			value: params.id,
			configurable: false,
			enumerable: false,
			writable: false,
		});
	}
}
