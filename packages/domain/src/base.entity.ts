import type { Identifiable } from './identifiable';

export abstract class BaseEntity<IdType = string> implements Identifiable<IdType> {
	public readonly id: IdType;

	protected constructor(params: Identifiable<IdType>) {
		this.id = params.id;
	}

	public equals<T extends BaseEntity<IdType>>(entity: T): boolean {
		return Object.getPrototypeOf(this)!.constructor === Object.getPrototypeOf(entity)!.constructor
			&& this.id === entity.id;
	}
}
