import { AggregateErrorException } from './aggregate-error.exception';
import {
	BaseException,
	ExceptionOptions,
	JSONBaseException,
	PAD_COUNT_PER_DEPTH,
} from './base.exception';

export type JSONBaseAggregateException = Omit<JSONBaseException, 'cause'> & {
	exceptions: JSONBaseException[];
};

export type BaseAggregateExceptionOptions = Omit<ExceptionOptions, 'cause'>;

export abstract class BaseAggregateException extends BaseException {
	public readonly exceptions: BaseException[];

	protected constructor(
		exceptions: BaseException[],
		message: string,
		options?: BaseAggregateExceptionOptions,
	) {
		super(message, options);

		this.exceptions = exceptions;
	}

	public override toJSON(): JSONBaseAggregateException {
		const baseJson = super.toJSON();

		return {
			...baseJson,
			exceptions: this.exceptions.map(exception => exception.toJSON()),
		};
	}

	public static override fromError(error: AggregateError): BaseAggregateException {
		return new AggregateErrorException(error);
	}

	protected override injectBeforeStack(padCount: number): string[] {
		const innerExceptionsInfo = this.exceptions
			.map(exception => exception['getStringifyFirstLine']())
			.map(line => BaseException.padLine(line, padCount));

		return [
			BaseException.padLine('Aggregated exceptions info:', padCount - PAD_COUNT_PER_DEPTH),
			...innerExceptionsInfo,
		];
	}
}
