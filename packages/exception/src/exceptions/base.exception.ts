import * as os from 'os';
import {
	InspectOptionsStylized,
	inspect,
} from 'util';

import { StackTraceFormatter } from '../utils';
import { ErrorException } from './error.exception';

export type JSONBaseException = {
	name: string;
	code: Nullable<number>;
	message: string;
	stack: string[];
	cause?: JSONBaseException;
};

export type ExceptionOptions = {
	cause?: BaseException | undefined;
	code?: number | undefined;
};

export const PAD_COUNT_PER_DEPTH = 4;

export abstract class BaseException extends Error {
	public readonly code: Nullable<number> = null;
	public override readonly stack: string;
	public override readonly message: string;
	public override readonly cause?: BaseException;
	private serialized: Nullable<JSONBaseException> = null;

	public constructor(message: string, codeOrOptions: ExceptionOptions = {}) {
		super(message);
		this.message = message;


		if (codeOrOptions.cause) {
			this.cause = codeOrOptions.cause;
		}
		if (codeOrOptions.code) {
			this.code = codeOrOptions.code;
		}

		Error.captureStackTrace(this, this.getCtor());
	}

	public override get name(): string {
		const { name } = this.getCtor();

		return name;
	}

	public toJSON(): JSONBaseException {
		if (this.serialized === null) {
			const { name, message, stack, code, cause } = this;

			this.serialized = {
				name,
				code,
				message,
				stack: StackTraceFormatter.format(stack, this.name),
			};

			if (cause) {
				this.serialized.cause = cause.toJSON();
			}
		}

		return this.serialized;
	}

	public override toString(padCount = PAD_COUNT_PER_DEPTH): string {
		const { stack } = this.toJSON();
		const formattedStack = stack.map(line => BaseException.padLine(line, padCount));

		const errorLines = [
			this.getStringifyFirstLine(),
			...this.injectBeforeStack(padCount + PAD_COUNT_PER_DEPTH),
			...formattedStack,
		];

		return errorLines.join(os.EOL);
	}

	// eslint-disable-next-line @typescript/unbound-method
	public [inspect.custom](_depth: Nullable<number>, { stylize }: InspectOptionsStylized): string {
		return stylize(this.toString(), 'regexp');
	}

	public isFromNextTick(): boolean {
		const lastLine = this.getLastStackLine();

		return lastLine.includes('processTicksAndRejections');
	}

	public isFromImmediate(): boolean {
		const lastLine = this.getLastStackLine();

		return lastLine.includes('Immediate._onImmediate');
	}

	public isFromTimer(): boolean {
		const lastLine = this.getLastStackLine();

		return lastLine.includes('Timeout._onTimeout');
	}

	public static fromError(error: Error): BaseException {
		return new ErrorException(error);
	}

	protected getCtor(): Function {
		return Object.getPrototypeOf(this)!.constructor;
	}

	protected getStringifyFirstLine(): string {
		const { name, code, message } = this.toJSON();

		return `${name}${code === null ? '' : ` [code: ${code}]`}: ${message}`;
	}

	protected injectBeforeStack(padCount: number): string[] {
		if (this.cause) {
			return [BaseException.padLine(this.cause.getStringifyFirstLine(), padCount)];
		}

		return [];
	}

	protected static padLine(line: string, padCount: number): string {
		return `${' '.repeat(padCount)}${line}`;
	}

	private getLastStackLine(): string {
		const { stack } = this.toJSON();

		return stack[stack.length - 1] ?? '';
	}
}
