import { BaseException } from './base.exception';

export class NotImplementedException extends BaseException {
	public constructor() {
		super('Not implemented');
	}
}
