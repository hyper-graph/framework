import { BaseException } from './base.exception';

export class ErrorException extends BaseException {
	public constructor(error: Error) {
		const { cause } = error as any;

		super(error.message, {
			cause: cause instanceof Error
				? new ErrorException(cause)
				// eslint-disable-next-line no-undefined
				: undefined,
		});

		(this as any).stack = error.stack;
	}
}
