import { BaseAggregateException } from './base.aggregate.exception';
import { BaseException } from './base.exception';

export class AggregateErrorException extends BaseAggregateException {
	public constructor(error: AggregateError) {
		const { errors } = error;

		super(
			errors
				.map(error => {
					if (error instanceof AggregateError) {
						BaseAggregateException.fromError(error);
					}
					if (error instanceof Error) {
						return BaseException.fromError(error);
					}

					return null;
				})
				.filter((exception): exception is BaseException => exception !== null),
			error.message,
		);

		(this as any).stack = error.stack;
	}
}
