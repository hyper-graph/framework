import { BaseException } from './base.exception';

export class UnknownException extends BaseException {
	public constructor(message: string) {
		super(message);
	}
}
