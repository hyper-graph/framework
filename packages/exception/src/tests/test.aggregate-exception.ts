import { BaseAggregateException } from '../exceptions';
import { TestException } from './test.exception';

export class TestAggregateException extends BaseAggregateException {
	public constructor(message: string, code?: number) {
		super(
			[
				new TestException('First inner exception'),
				new TestException('Second inner exception'),
				new TestException('Third inner exception'),
			],
			message,
			{ code },
		);
	}
}
