import '@hg-ts/types';
import {
	Test,
	TestSuite,
	Describe,
	expect,
} from '@hg-ts/tests';
import {
	BaseAggregateException,
	BaseException,
	ErrorException,
} from '../exceptions';
import { TestAggregateException } from './test.aggregate-exception';
import { TestException } from './test.exception';

@Describe()
export class SerializationTestSuite extends TestSuite {
	@Test()
	public async commonTest(): Promise<void> {
		const message = 'Test message';
		const exception = new TestException(message);

		expect(exception).toBeInstanceOf(Error);
		expect(exception).toBeInstanceOf(BaseException);
		expect(exception).toBeInstanceOf(TestException);
		expect(exception.message).toEqual(message);
		expect(exception.code).toBeNull();
		expect(exception.toJSON().name).toEqual(TestException.name);
		expect(exception.toJSON().stack).toBeInstanceOf(Array);
		expect(exception.toJSON().stack).not.toEqual([]);
	}

	@Test()
	public async baseExceptionFromError(): Promise<void> {
		const message = 'Test message';
		const error = new Error(message);
		const exception = BaseException.fromError(error);

		expect(exception).toBeInstanceOf(Error);
		expect(exception).toBeInstanceOf(BaseException);
		expect(exception).toBeInstanceOf(ErrorException);
		expect(exception.message).toEqual(message);
		expect(exception.code).toBeNull();
		expect(exception.toJSON().stack).toBeInstanceOf(Array);
		expect(exception.toJSON().stack).not.toEqual([]);
	}

	@Test()
	public async aggregateExceptionTest(): Promise<void> {
		const message = 'Test aggregate message';
		const exception = new TestAggregateException(message);

		expect(exception).toBeInstanceOf(Error);
		expect(exception).toBeInstanceOf(BaseException);
		expect(exception).toBeInstanceOf(BaseAggregateException);
		expect(exception).toBeInstanceOf(TestAggregateException);

		expect(exception.message).toEqual(message);
		expect(exception.code).toBeNull();

		expect(exception.exceptions).toBeInstanceOf(Array);
		expect(exception.exceptions).toHaveLength(3);
	}

	@Test()
	public async syncStackTest(): Promise<void> {
		const { stack } = new TestException('Test message').toJSON();

		expect(stack[0]!.startsWith('at SerializationTestSuite.syncStackTest')).toBeTruthy();
	}

	@Test()
	public async isFromNextTickTest(): Promise<void> {
		return new Promise<void>(resolve => {
			process.nextTick(() => {
				const exception = new TestException('Test message');

				expect(exception.isFromNextTick()).toBeTruthy();
				resolve();
			});
		});
	}

	@Test()
	public async isFromImmediateTest(): Promise<void> {
		return new Promise<void>(resolve => {
			setImmediate(() => {
				const exception = new TestException('Test message');

				expect(exception.isFromImmediate()).toBeTruthy();
				resolve();
			});
		});
	}

	@Test()
	public async fromTimeoutTest(): Promise<void> {
		return new Promise<void>(resolve => {
			setTimeout(() => {
				const exception = new TestException('Test message');

				expect(exception.isFromTimer()).toBeTruthy();
				resolve();
			}, 0);
		});
	}

	@Test()
	public async fromIntervalTest(): Promise<void> {
		let interval: NodeJS.Timeout;
		return new Promise<void>(resolve => {
			interval = setInterval(() => {
				const exception = new TestException('Test message');

				expect(exception.isFromTimer()).toBeTruthy();
				clearInterval(interval);
				resolve();
			}, 0);
		});
	}
}
