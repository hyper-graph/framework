import os from 'os';
import { SOURCE_MAP_ENABLED } from '../consts';

export class StackTraceFormatter {
	public static format(stack: string, exceptionName: string): string[] {
		return StackTraceFormatter.getStackLines(stack, exceptionName);
	}

	protected static getStackLines(stack: string, exceptionName: string): string[] {
		let lines = stack.split(os.EOL)
			.splice(1)
			.filter(stackLine => !StackTraceFormatter.isInternalStackLine(stackLine))
			.map(line => line.trim());

		if (SOURCE_MAP_ENABLED) {
			lines = lines.slice(4);
		}
		const lineFromExceptionConstructor = lines.findIndex(line => line.startsWith(`at new ${exceptionName}`));

		if (lineFromExceptionConstructor > -1) {
			return lines.slice(lineFromExceptionConstructor + 1);
		}

		return lines;
	}

	protected static isInternalStackLine(line: string): boolean {
		return line.includes('internal/') && !line.includes('processTicksAndRejections');
	}
}
