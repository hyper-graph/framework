import { ESLint } from 'eslint';
import isCI from 'is-ci';
import yargs from 'yargs';
import eslintFormatterGitLab from 'eslint-formatter-gitlab';

const rawArgs = process.argv.slice(2);

const options = yargs(rawArgs)
	.boolean('fix')
	.parse(rawArgs);

const path = options._[0] ?? '.';

const linter = new ESLint({
	cache: !isCI,
});

const results = await linter.lintFiles(path);

if (options.fix) {
	await ESLint.outputFixes(results);
}

/**
 *
 * @returns {ESLint.Formatter}
 */
function getCIFormatter() {
	return {
		/**
		 * @param {Linter.LintResult[]} results
		 * @return string
		 */
		format: function(results, meta) {
			let rulesMeta;

			return eslintFormatterGitLab(results, {
				...meta,
				cwd: process.cwd(),
				get rulesMeta() {
					if (!rulesMeta) {
						rulesMeta = linter.getRulesMetaForResults(results);
					}

					return rulesMeta;
				}
			})
		}
	}
}

function getLocalFormatter() {
	return linter.loadFormatter('stylish');
}

const formatter = isCI ? getCIFormatter() : await getLocalFormatter();

const resultText = await formatter.format(results);

if (resultText) {
	console.log(resultText);
}
