import { BaseException } from '@hg-ts/exception';

export class SerializeException extends BaseException {
	public constructor(type: string) {
		super(`Unsupported type for serialization: ${type}`);
	}
}
