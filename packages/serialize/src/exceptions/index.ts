export * from './deserialize-unknown-prototype.exception';
export * from './deserialize.exception';
export * from './serialize.exception';
