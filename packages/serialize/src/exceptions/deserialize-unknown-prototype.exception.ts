import { BaseException } from '@hg-ts/exception';

export class DeserializeUnknownPrototypeException extends BaseException {
	public constructor(ctorName: string) {
		super(`Deserialize error. Constructor with name ${ctorName} not found`);
	}
}
