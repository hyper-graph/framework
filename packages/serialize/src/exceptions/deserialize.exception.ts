import { BaseException } from '@hg-ts/exception';

import type { SerializedValue } from '../types';

export class DeserializeException extends BaseException {
	public constructor(value: SerializedValue) {
		super(`Error for deserialize value: ${JSON.stringify(value)}`);
	}
}
