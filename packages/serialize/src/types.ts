export type SerializedNumber = {
	type: 'number';
	value: number;
};

export type SerializedString = {
	type: 'string';
	value: string;
};

export type SerializedBoolean = {
	type: 'boolean';
	value: boolean;
};

export type SerializedBigint = {
	type: 'bigint';
	value: string;
};

export type SerializedDate = {
	type: 'date';
	value: number;
};

export type SerializedArray = {
	type: 'array';
	value: SerializedValue[];
};

export type SerializedBuffer = {
	type: 'buffer';
	value: string;
};

export type SerializedObject = {
	type: 'object';
	prototypeNameList: string[];
	value: Record<string, SerializedValue>;
};

export type SerializedMap = {
	type: 'map';
	value: {
		key: SerializedValue;
		value: SerializedValue;
	}[];
};

export type SerializedSet = {
	type: 'set';
	value: SerializedValue[];
};

export type SerializedNull = {
	type: 'null';
};

export type SerializedValue =
	| SerializedArray
	| SerializedBigint
	| SerializedBoolean
	| SerializedDate
	| SerializedMap
	| SerializedNull
	| SerializedBuffer
	| SerializedNumber
	| SerializedObject
	| SerializedSet
	| SerializedString;
