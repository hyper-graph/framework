import { DeserializeException, DeserializeUnknownPrototypeException } from './exceptions';
import type {
	SerializedObject,
	SerializedValue,
} from './types';
import { classes } from './additional-classes';

function deserializeObject(
	{ value, prototypeNameList }: SerializedObject,
): unknown {
	let targetClass: Class = Object;
	const [preferredClassName = null] = prototypeNameList.filter(name => name !== Object.name);

	if (preferredClassName) {
		const foundCtor = classes.get(preferredClassName);

		if (!foundCtor) {
			throw new DeserializeUnknownPrototypeException(preferredClassName);
		}

		targetClass = foundCtor;
	}

	const result: Record<string, unknown> = Object.create(targetClass.prototype);

	return Object.keys(value)
		.reduce<Record<string, unknown>>((result, key) => {
			result[key] = deserializeValue(value[key]!);

			return result;
		}, result);
}

function deserializeValue(value: SerializedValue): unknown {
	switch (value.type) {
	case 'boolean':
	case 'string':
	case 'number':
		return value.value;
	case 'buffer':
		return Buffer.from(value.value, 'hex');
	case 'bigint':
		return BigInt(value.value);
	case 'null':
		return null;
	case 'date':
		return new Date(value.value);
	case 'array':
		return value.value.map(item => deserializeValue(item));
	case 'set':
		return new Set(value.value.map(item => deserializeValue(item)));
	case 'map':
		return new Map(value.value.map(item => [
			deserializeValue(item.key),
			deserializeValue(item.value),
		]));
	case 'object':
		return deserializeObject(value);

	default: {
		const error: never = value;
		throw new DeserializeException(error);
	}
	}
}

export function deserialize(value: SerializedValue): unknown {
	return deserializeValue(value);
}
