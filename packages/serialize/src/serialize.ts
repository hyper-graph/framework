import type {
	SerializedArray,
	SerializedObject,
	SerializedValue,
} from './types';
import { SerializeException } from './exceptions';

export function getPrototypeListNames(value: Record<string | symbol, unknown>): string[] {
	const result: string[] = [];
	let nextPrototype = Object.getPrototypeOf(value);

	while (nextPrototype !== null) {
		result.push(nextPrototype.constructor.name);

		nextPrototype = Object.getPrototypeOf(nextPrototype);
	}

	return result;
}

function serializeArray(value: unknown[]): SerializedArray {
	return {
		type: 'array',
		value: value.map(item => serializeValue(item)),
	};
}

function serializeRecord(value: Record<string | symbol, unknown>): SerializedObject {
	const fields = Object.keys(value)
		.reduce<Record<string, SerializedValue>>((result, key) => {
			if (typeof key === 'symbol') {
				throw new SerializeException('symbol key');
			}
			result[key] = serializeValue(value[key]);
			return result;
		}, {});

	return {
		type: 'object',
		prototypeNameList: getPrototypeListNames(value),
		value: fields,
	};
}

function serializeObject(value: object | null): SerializedValue {
	if (value === null) {
		return { type: 'null' };
	}
	if (value instanceof Date) {
		return {
			type: 'date',
			value: value.getTime(),
		};
	}
	if (value instanceof Buffer) {
		return {
			type: 'buffer',
			value: value.toString('hex'),
		};
	}
	if (value instanceof Map) {
		return {
			type: 'map',
			value: [...value.entries()].map(([key, value]) => ({
				key: serializeValue(key),
				value: serializeValue(value),
			})),
		};
	}
	if (value instanceof Set) {
		return {
			type: 'set',
			value: [...value.values()].map(item => serializeValue(item)),
		};
	}
	if (Array.isArray(value)) {
		return serializeArray(value);
	}

	return serializeRecord(value as any);
}

function serializeValue(value: unknown): SerializedValue {
	switch (typeof value) {
	case 'boolean':
		return {
			value,
			type: 'boolean',
		};
	case 'bigint':
		return {
			type: 'bigint',
			value: String(value),
		};
	case 'string':
		return {
			value,
			type: 'string',
		};
	case 'number':
		return {
			value,
			type: 'number',
		};
	case 'object':
		return serializeObject(value as any);
	default:
		throw new SerializeException(typeof value);
	}
}

export function serialize(value: unknown): SerializedValue {
	return serializeValue(value);
}
