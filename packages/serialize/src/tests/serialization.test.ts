import '@hg-ts/types';
import {
	Test,
	TestSuite,
	Describe,
	expect,
} from '@hg-ts/tests';
import { serialize } from '../serialize';
import { deserialize } from '../deserialize';

@Describe()
export class SerializationTestSuite extends TestSuite {
	@Test()
	public async serializeNull(): Promise<void> {
		this.test(null);
	}

	@Test()
	public async serializeNumber(): Promise<void> {
		this.test(123);
		this.test(12.35);
		this.test(0);
		this.test(-12.35);
		this.test(-123);
	}

	@Test()
	public async serializeBigint(): Promise<void> {
		this.test(1235n * 1_000_000_000_000_000_000_000_000n);
		this.test(1235n);
		this.test(123n);
		this.test(0n);
		this.test(-123n);
		this.test(-1235n * 1_000_000_000_000_000_000_000_000n);
	}

	@Test()
	public async serializeBuffer(): Promise<void> {
		this.test(Buffer.from('sdfsdgdfgdfg'));
	}

	protected test(expected: unknown): void {
		const value = deserialize(serialize(expected));

		expect(value).toEqual(expected);
	}
}
