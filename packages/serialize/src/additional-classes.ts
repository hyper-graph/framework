export const classes = new Map<string, Class>();

export function addAdditionalClass(classToTransform: Class): void {
	classes.set(classToTransform.name, classToTransform);
}
