const { resolve } = require('path');

module.exports.baseNodeOptions = [
	'enable-source-maps',
	'abort-on-uncaught-exception',
];

const baseConfig = {
	'async-only': true,
	extension: [
		'ts',
		'tsx',
	],
	spec: [
		'dist/**/*.test.js',
		'dist/**/*.test-suite.js',
		'dist/**/*TestSuite.js',
	],
	'trace-warnings': true,
	checkLeaks: true,
	exit: true,
	file: [require.resolve('reflect-metadata')]
};

if (process.env.CI) {
	baseConfig.reporter = 'mocha-junit-reporter';
	baseConfig['reporter-options'] = `mochaFile=${resolve('mocha.junit.xml')}`;
}

module.exports.baseConfig = baseConfig;
