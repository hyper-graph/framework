const { baseNodeOptions, baseConfig } = require('./base.cjs');

module.exports = {
	...baseConfig,
	'node-option': baseNodeOptions.join(','),
};
