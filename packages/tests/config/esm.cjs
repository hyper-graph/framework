const { baseNodeOptions, baseConfig } = require('./base.cjs');

const nodeOptions = [
	...baseNodeOptions,
	'experimental-import-meta-resolve',
	'experimental-json-modules',
	'experimental-specifier-resolution=node',
	'no-warnings'
];

module.exports = {
	...baseConfig,
	'node-option': nodeOptions.join(','),
};
