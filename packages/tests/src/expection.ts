import expectFunc from 'expect';

(global as any).expect = expectFunc;

declare global {
    const expect: typeof expectFunc;
}

declare namespace jest {
    export type Mock = any;
}

export { expectFunc as expect };
