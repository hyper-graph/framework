import { expect } from './expection';
import type {
	ExclusiveSuiteFunction,
	ExclusiveTestFunction,
	PendingSuiteFunction,
	PendingTestFunction,
	SuiteFunction,
	TestFunction,
} from 'mocha';
import type {
	MethodName,
	Options,
} from './decorators';
import type { TestSuite, TestSuiteConstructor } from './test-suite';

export type TestRunnerOptions = {
	name?: string;
	only?: boolean;
	skip?: boolean;
};

export type TestMethod = (...args: unknown[]) => Promise<void>;
type Describe = ExclusiveSuiteFunction | PendingSuiteFunction | SuiteFunction;
type ItFunction = ExclusiveTestFunction | PendingTestFunction | TestFunction;

export class TestRunner {
	public static run(
		suiteConstructor: TestSuiteConstructor,
		options: TestRunnerOptions = {},
	): void {
		const suite = new suiteConstructor();
		suite.testsMap = suiteConstructor.prototype.testsMap;
		const { name = suite.constructor.name } = options;

		const describeFunction = this.getDescribeFunction(options);

		describeFunction(name, this.describe.bind(this, suite));
	}

	private static getDescribeFunction(options: TestRunnerOptions = {}): Describe {
		const {
			only = false,
			skip = false,
		} = options;

		let describeFunction: Describe = describe;

		if (only) {
			describeFunction = describe.only;
		} else if (skip) {
			describeFunction = describe.skip;
		}

		return describeFunction;
	}

	private static describe(suite: TestSuite): void {
		before(async() => suite.setUp());
		after(async() => suite.tearDown());

		beforeEach(async() => suite.beforeEach());
		afterEach(async() => {
			expect.setState({
				assertionCalls: 0,
				expectedAssertionsNumber: null,
				isExpectingAssertions: false,
			});
			await suite.afterEach();
		});

		if (!suite.testsMap) {
			suite.testsMap = new Map();
		}

		suite.testsMap.forEach((options, methodName) => this.registerTests(suite, options, methodName));
	}

	private static registerTests(suite: TestSuite, options: Options, testName: MethodName): void {
		const { name = testName.toString() } = options;

		if (options.todo) {
			it.skip(`TODO: ${name}`);
			return;
		}

		const method = this.getTestMethod(suite, testName);

		const registerMethod = this.getTestRegisterMethod(options);

		registerMethod(name, method);
	}

	private static getTestMethod(suite: TestSuite, methodName: MethodName): TestMethod {
		const testMethod = ((suite as any)[methodName] as (...args: unknown[]) => Promise<void>).bind(suite);

		return async(...args: any[]): Promise<void> => {
			await testMethod(...args);
			const { expectedAssertionsNumber, assertionCalls } = expect.getState();

			if (typeof expectedAssertionsNumber === 'number') {
				expect(assertionCalls).toBe(expectedAssertionsNumber);
			}
		};
	}

	private static getTestRegisterMethod(options: Options): ItFunction {
		let registerMethod: ItFunction = it;

		if (options.skip) {
			registerMethod = it.skip;
		} else if (options.only) {
			registerMethod = it.only;
		}

		return registerMethod;
	}
}
