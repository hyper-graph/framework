import { expect } from '../expection';
import { createTestDecorator } from './decorate-test';

export function ExpectException(error: Class<any, any[]>): MethodDecorator {
	return createTestDecorator(async(method: () => Promise<void>) => {
		let isThrowError = false;

		try {
			await method();
		} catch (err: unknown) {
			isThrowError = true;
			expect(err).toBeInstanceOf(error);
		} finally {
			expect(isThrowError).toBeTruthy();
		}
	});
}
