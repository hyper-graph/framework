import {
	TestRunner,
	TestRunnerOptions,
} from '../test-runner';

export function Describe(options: TestRunnerOptions = {}): ClassDecorator {
	return (target: any): void => {
		TestRunner.run(target, options);
	};
}
