type TestDecorator = (callback: () => Promise<void>) => Promise<void>;

export function createTestDecorator(decorator: TestDecorator): MethodDecorator {
	function result(
		_target: unknown,
		_key: string,
		descriptor: TypedPropertyDescriptor<(...args: unknown[]) => Promise<void>>,
	): TypedPropertyDescriptor<(...args: unknown[]) => Promise<void>> {
		return {
			async value(...args: unknown[]): Promise<void> {
				// eslint-disable-next-line @typescript/no-this-alias
				const self = this;
				const method = descriptor.value;
				if (typeof method === 'function') {
					await decorator(method.bind(self, ...args));
				}
			},
		};
	}

	return result as MethodDecorator;
}
