import type { TestSuite } from '../test-suite';


export type MethodName = string | symbol;
export type Options = {
	name?: string;
	only?: boolean;
	skip?: boolean;
	todo?: boolean;
};
export type Method<Arguments extends any[] = never> = (...args: Arguments) => Promise<void> | void;

export function Test(options: Options = {}): MethodDecorator {
	return (
		target: Object,
		propertyKey: MethodName,
	): void => {
		const suite = target as TestSuite;

		suite.testsMap ??= new Map();
		suite.testsMap.set(propertyKey, options);
	};
}
