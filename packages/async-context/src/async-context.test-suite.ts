import {
	Describe,
	expect,
	ExpectException,
	Test,
	TestSuite,
} from '@hg-ts/tests';
import {
	Subject,
	Subscription,
	tap,
} from 'rxjs';

import { AsyncContextNotFoundException } from './async-context.not-found.exception';
import { AsyncContextProvider } from './async-context.provider';

type OperatorTestSubscription = {
	subscription: Subscription;
	subject: Subject<number>;
	input: number;
	context: number;
};

@Describe()
export class AsyncContextTestSuite extends TestSuite {
	private context: AsyncContextProvider<number>;

	@Test()
	public async emptyContext(): Promise<void> {
		expect(this.context.get()).toBe(null);
	}

	@Test()
	public async noPromiseContext(): Promise<void> {
		const contextValue = Math.random();
		const resultValue = Math.random();

		const result = this.context.run(contextValue, () => {
			expect(this.context.getOrFail()).toBe(contextValue);

			this.context.exit(() => {
				expect(this.context.get()).toBe(null);
			});
			return resultValue;
		});

		expect(result).toBe(resultValue);
	}

	@Test()
	public async promiseContext(): Promise<void> {
		const contextValue = Math.random();
		const resultValue = Math.random();

		const result = await this.context.run(contextValue, async() => {
			expect(this.context.getOrFail()).toBe(contextValue);

			await this.context.exit(async() => {
				expect(this.context.get()).toBe(null);
			});
			return resultValue;
		});

		expect(result).toBe(resultValue);
	}

	@Test()
	public async operatorContext(): Promise<void> {
		const { subject, input } = this.createOperatorTestSubscription();

		subject.next(input);
		subject.complete();

		expect.assertions(4);
	}

	@Test()
	public async operatorWithFactory(): Promise<void> {
		const { subject, input } = this.createOperatorTestSubscription(value => -value);

		subject.next(input);
		subject.complete();

		expect.assertions(4);
	}

	@Test()
	public async operatorWithErrorContext(): Promise<void> {
		const { subject, input } = this.createOperatorTestSubscription();

		subject.error(input);
		subject.complete();

		expect.assertions(4);
	}

	@Test()
	public async operatorAfterUnsubscribe(): Promise<void> {
		const { subject, input, subscription } = this.createOperatorTestSubscription();

		subscription.unsubscribe();
		subject.next(input);
		subject.complete();

		expect.assertions(2);
	}

	@Test()
	public async operatorWithErrorAfterUnsubscribe(): Promise<void> {
		const { subject, input, subscription } = this.createOperatorTestSubscription();

		subscription.unsubscribe();
		subject.error(input);
		subject.complete();

		expect.assertions(2);
	}

	@Test()
	@ExpectException(AsyncContextNotFoundException)
	public async contextNotFound(): Promise<void> {
		this.context.getOrFail();
	}

	public override async beforeEach(): Promise<void> {
		this.context = new AsyncContextProvider();
	}

	private createOperatorTestSubscription(factory?: (input: number) => number): OperatorTestSubscription {
		const subject = new Subject<number>();
		const input = Math.random();
		const context = factory ? factory(input) : input;

		const withContext = (value: number): void => {
			expect(value).toBe(input);
			expect(this.context.getOrFail()).toBe(context);
		};

		const withoutContext = (value: number): void => {
			expect(value).toBe(input);
			expect(this.context.get()).toBe(null);
		};

		const subscription = subject.asObservable()
			.pipe(
				this.context.runOperator(factory),
				tap({
					next: withContext,
					error: withContext,
				}),
				this.context.exitOperator(),
				tap({
					next: withoutContext,
					error: withoutContext,
				}),
			)
			.subscribe({ next() {}, error() {} });

		return { subject, subscription, input, context };
	}
}
