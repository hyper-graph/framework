import { BaseException } from '@hg-ts/exception';

export class AsyncContextNotFoundException extends BaseException {
	public constructor() {
		super('Async context not provided');
	}
}
