import { AsyncLocalStorage } from 'node:async_hooks';
import {
	MonoTypeOperatorFunction,
	Observable,
} from 'rxjs';
import { AsyncContextNotFoundException } from './async-context.not-found.exception';

type AsyncContextProviderCallback<T> = () => T;
type AsyncContextFactory<InputType, Context> = (input: InputType) => Context;

type NextCallback<T> = (value: T) => void;

export class AsyncContextProvider<Context> {
	protected readonly storage = new AsyncLocalStorage<Context>();

	public get(): Nullable<Context> {
		return this.storage.getStore() ?? null;
	}

	public getOrFail(): NonNullable<Context> {
		const context = this.get();

		if (typeof context === 'undefined' || context === null) {
			throw new AsyncContextNotFoundException();
		}

		return context as NonNullable<Context>;
	}

	public run<ReturnType>(
		context: Context,
		callback: AsyncContextProviderCallback<ReturnType>,
	): ReturnType {
		return this.storage.run(context, callback);
	}

	public exit<ReturnType>(
		callback: AsyncContextProviderCallback<ReturnType>,
	): ReturnType {
		return this.storage.exit(callback);
	}

	public runOperator<T>(
		factory?: AsyncContextFactory<T, Context>,
	): MonoTypeOperatorFunction<T> {
		return this.createOperator<T>((next, value) => {
			const context = factory ? factory(value) : value as any;
			this.storage.run(context, next);
		});
	}

	public exitOperator<T>(): MonoTypeOperatorFunction<T> {
		return this.createOperator(next => this.storage.exit(next));
	}

	private createOperator<T>(onNext: (next: () => void, value: T) => void): MonoTypeOperatorFunction<any> {
		return (source): Observable<T> => new Observable<T>(subscriber => {
			const callNext: NextCallback<T> = input => {
				const next = (): void => {
					if (!subscriber.closed) {
						subscriber.next(input);
					}
				};
				onNext(next, input);
			};
			const callError: NextCallback<T> = input => {
				const next = (): void => {
					if (!subscriber.closed) {
						subscriber.error(input);
					}
				};
				onNext(next, input);
			};

			source.subscribe({
				next: callNext,
				error: callError,
				complete: () => subscriber.complete(),
			});
		});
	}
}
