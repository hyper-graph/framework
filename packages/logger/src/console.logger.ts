import winston from 'winston';
import { MESSAGE } from 'triple-beam';
import {
	baseFormatter,
	createFormatter,
	LogMessage,
} from './base.formatter';
import {
	BaseLogger,
	LogLevel,
} from './base.logger';
import {
	COLOR,
	colorize,
} from './colors';

export const colorsMap = new Map<LogLevel, COLOR>()
	.set('emergency', COLOR.RED)
	.set('alert', COLOR.RED)
	.set('critical', COLOR.RED)
	.set('error', COLOR.RED)
	.set('warning', COLOR.YELLOW)
	.set('notice', COLOR.GREEN)
	.set('info', COLOR.CYAN_BRIGHT)
	.set('debug', COLOR.MAGENTA_BRIGHT);

function colorizeLevel(level: LogLevel): string {
	const levelColor = colorsMap.get(level)!;

	return `<${colorize(levelColor, level)}>`;
}

function colorizeContext(context: Nullable<string>): string {
	if (context) {
		return `[${colorize(COLOR.ORANGE, context)}]`;
	}

	return '';
}

function colorizeMessage(level: LogLevel, message: string): string {
	const levelColor = colorsMap.get(level)!;

	return colorize(levelColor, message);
}

function formatProcess(process: LogMessage['process']): string {
	if (process.title) {
		return `[${process.title}: ${process.pid}]`;
	}
	return `[pid: ${process.pid}]`;
}

function formatTime(time: Date): string {
	return time.toLocaleString('ru-RU');
}

const customFormat = createFormatter(info => {
	const process = formatProcess(info.process);
	const timestamp = formatTime(info.timestamp);
	const context = colorizeContext(info.context);
	const level = colorizeLevel(info.level);
	const message = colorizeMessage(info.level, info.message);

	const metaInfoItems: string[] = [timestamp, context, level, process];

	const metaInfo = metaInfoItems.join(' ');

	info[MESSAGE] = `${metaInfo}: ${message}`;

	return info;
});

const consoleTransport = new winston.transports.Console({
	format: winston.format.combine(
		baseFormatter(),
		customFormat(),
	),
});

export class ConsoleLogger extends BaseLogger {
	public constructor() {
		super('debug', 'test');

		this.addTransport(consoleTransport);
	}
}
