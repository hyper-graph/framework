import winston from 'winston';
import { baseFormatter } from './base.formatter';
import { BaseLogger } from './base.logger';

const jsonTransport = new winston.transports.Console({
	format: winston.format.combine(
		baseFormatter(),
		winston.format.json(),
	),
});

export class JSONLogger extends BaseLogger {
	public constructor() {
		super('debug', 'test');

		this.addTransport(jsonTransport);
	}
}
