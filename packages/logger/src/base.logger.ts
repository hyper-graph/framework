import { format } from 'node:util';
import winston from 'winston';

import { Logger } from './logger';

type Transport = winston.Logger['add'] extends (transport: infer R) => void ? R : never;

export type LogLevel = keyof Omit<Logger, 'setContext'>;

export const syslogToLoggerMap = new Map<string, LogLevel>()
	.set('emerg', 'emergency')
	.set('crit', 'critical');

export const loggerToSyslogMap = new Map<LogLevel, string>()
	.set('emergency', 'emerg')
	.set('critical', 'crit');


export abstract class BaseLogger implements Logger {
	protected context: Nullable<string> = null;
	private readonly logger: winston.Logger;

	protected constructor(logLevel: LogLevel, context: string) {
		this.context = context;

		this.logger = winston.createLogger({
			level: logLevel,
			levels: winston.config.syslog.levels,
			transports: [],
			handleExceptions: true,
			handleRejections: true,
			exitOnError: false,
		});
	}

	public debug(...messages: unknown[]): void {
		this.log('debug', messages);
	}
	public info(...messages: unknown[]): void {
		this.log('info', messages);
	}
	public notice(...messages: unknown[]): void {
		this.log('notice', messages);
	}
	public warning(...messages: unknown[]): void {
		this.log('warning', messages);
	}
	public error(...messages: unknown[]): void {
		this.log('error', messages);
	}
	public critical(...messages: unknown[]): void {
		this.log('critical', messages);
	}
	public alert(...messages: unknown[]): void {
		this.log('alert', messages);
	}
	public emergency(...messages: unknown[]): void {
		this.log('emergency', messages);
	}

	public setContext(context: Nullable<string>): void {
		this.context = context;
	}

	protected addTransport(transport: Transport): void {
		this.logger.add(transport);
	}

	private log(level: LogLevel, messages: unknown[]): void {
		const mappedLevel = loggerToSyslogMap.get(level) ?? level;

		this.logger.log({
			level: mappedLevel,
			message: format(...messages),
			items: messages,
			context: this.context,
		});
	}
}
