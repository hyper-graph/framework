export abstract class Logger {
	public abstract debug(...messages: unknown[]): void;
	public abstract info(...messages: unknown[]): void;
	public abstract notice(...messages: unknown[]): void;
	public abstract warning(...messages: unknown[]): void;
	public abstract error(...messages: unknown[]): void;
	public abstract critical(...messages: unknown[]): void;
	public abstract alert(...messages: unknown[]): void;
	public abstract emergency(...messages: unknown[]): void;

	public abstract setContext(context: Nullable<string>): void;
}
