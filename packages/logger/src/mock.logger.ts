import { Logger } from './logger';

export class MockLogger extends Logger {
	public debug(): void {}
	public info(): void {}
	public notice(): void {}
	public warning(): void {}
	public error(): void {}
	public critical(): void {}
	public alert(): void {}
	public emergency(): void {}

	public setContext(): void {}
}
