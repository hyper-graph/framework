import * as assert from 'node:assert';
import winston from 'winston';
import {
	LogLevel,
	syslogToLoggerMap,
} from './base.logger';

export type LogMessage = winston.Logform.TransformableInfo & {
	context: Nullable<string>;
	message: string;
	level: LogLevel;
	process: {
		pid: number;
		title: Nullable<string>;
	};
	timestamp: Date;
	items: unknown[];
};

function getLogData(info: winston.Logform.TransformableInfo): LogMessage {
	const level = syslogToLoggerMap.get(info.level) ?? info.level as LogLevel;
	const context = info['context'] ?? null;

	assert.ok(typeof context === 'string' || context === null);

	return {
		level,
		context,
		process: {
			pid: process.pid,
			title: process.title.endsWith('node') ? null : process.title,
		},
		message: info.message as string,
		items: Array.isArray(info['items']) ? info['items'] : [info['items']],
		timestamp: new Date(),
	};
}

export const baseFormatter = winston.format(info => {
	const logData = getLogData(info);

	logData['items'] = logData.items
		.map(item => {
			if (typeof item === 'bigint') {
				return `${item}n`;
			}

			if (item instanceof Error && !('toJSON' in item)) {
				return {
					name: item.name,
					message: item.message,
					stack: item.stack,
				};
			}

			return item;
		});
	return logData;
});

export function createFormatter(formatter: (info: LogMessage) => LogMessage): winston.Logform.FormatWrap {
	return winston.format(formatter as any);
}
