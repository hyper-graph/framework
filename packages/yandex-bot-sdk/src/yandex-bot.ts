import axios, { AxiosInstance } from 'axios';
import { Subject } from 'rxjs';
import type { Message } from './message';
import { Poll } from './poll';

type BaseResponse = {
	ok: boolean;
};

type SendMessageResponse = BaseResponse & {
	message_id: number;
};

type Chat = {
	type: 'channel' | 'group' | 'private';
	id?: string;
};

export type UpdateData = {
	message_id: number;
	timestamp: number;
	chat: Chat;
	from: {
		id: string;
		display_name: string;
		login: string;
		robot: boolean;
	};
	update_id: number;
	text: string;
};

type GetUpdatesResponse = BaseResponse & {
	updates: UpdateData[];
};

type GetPollUpdatesResponse = BaseResponse & {
	voted_count: number;
	answers: Record<string, number>;
};

type BaseSendRequest = {
	chat_id?: string;
	login?: string;
	payload_id?: string;
	reply_message_id?: number;
	disable_notification?: boolean;
	important?: boolean;
	disable_web_page_preview?: boolean;
	thread_id?: number;
};
type SendMessageRequest = BaseSendRequest & {
	text: string;
};
type SendPollRequest = BaseSendRequest & {
	is_anonymous?: boolean;
	max_choices?: number;
	answers: string[];
	title: string;
};

export class YandexBot {
	private readonly client: AxiosInstance;
	private lastUpdateId = 0;
	private readonly messagesPipe = new Subject();
	private readonly subscribedPoll = new Set<Poll>();

	public constructor(token: string) {
		this.client = axios.create({
			baseURL: 'https://botapi.messenger.yandex.net/bot/v1',
			headers: { Authorization: `OAuth ${token}` },
		});

		// eslint-disable-next-line @typescript/no-misused-promises
		setInterval(async() => this.getNewMessages(), 1000);
		// eslint-disable-next-line @typescript/no-misused-promises
		setInterval(async() => this.getPollResults(), 1000);
	}

	public async send(message: Message): Promise<void> {
		const baseRequest = this.getBaseParams(message);

		let params: SendMessageRequest | SendPollRequest;
		let method: 'createPoll' | 'sendText';

		if (message instanceof Poll) {
			params = {
				...baseRequest,
				title: message['text'],
				answers: message['answers'],
			};
			method = 'createPoll';
		} else {
			params = {
				...baseRequest,
				text: message['text'],
			};
			method = 'sendText';
		}

		const { data } = await this.client.post<SendMessageResponse>(`/messages/${method}/`, params);

		message['setId'](data.message_id);

		if (message instanceof Poll) {
			message['isSubscribed'] = true;
			this.subscribedPoll.add(message);
		}
	}

	private async getNewMessages(): Promise<void> {
		const { data } = await this.client.post<GetUpdatesResponse>('/messages/getUpdates/', { offset: this.lastUpdateId });
		const { updates } = data;

		if (updates.length === 0) {
			return;
		}

		updates.forEach(item => this.messagesPipe.next(item));

		this.lastUpdateId = data.updates[data.updates.length - 1]!.update_id + 1;
	}

	private async getPollResults(): Promise<void> {
		const polls = [...this.subscribedPoll.values()];
		await Promise.all(polls.map(async item => this.getSinglePollResult(item)));
	}

	private async getSinglePollResult(poll: Poll): Promise<void> {
		if (!poll['isSubscribed']) {
			this.subscribedPoll.delete(poll);
			return;
		}

		const { data } = await this.client.get<GetPollUpdatesResponse>('/polls/getResults/', {
			params: {
				// eslint-disable-next-line camelcase
				message_id: poll['id']!,
				// eslint-disable-next-line no-undefined
				login: poll.loginRecipient() ?? undefined,
				// eslint-disable-next-line camelcase, no-undefined
				chat_id: poll.chatRecipient() ?? undefined,
			},
		});

		// eslint-disable-next-line prefer-destructuring
		const votedCount = poll['votedCount'];

		if (votedCount === data.voted_count) {
			return;
		}

		this.handlePollResponse(poll, data);
	}

	private handlePollResponse(poll: Poll, data: GetPollUpdatesResponse): void {
		// eslint-disable-next-line prefer-destructuring
		const votes = poll['votes'];
		// eslint-disable-next-line prefer-destructuring
		const answers = poll['answers'];
		const mappedAnswers = new Map<string, number>();

		Object.entries(data.answers).forEach(([key, value]) => {
			const answer = answers[Number(key) - 1]!;

			mappedAnswers.set(answer, value);
		});

		mappedAnswers.forEach((count, answer) => {
			const oldValue = votes.get(answer);

			if (count !== oldValue && count !== 0) {
				poll['receiveAnswer'](answer);
			}
		});

		poll['votes'] = mappedAnswers;
		poll['votedCount'] = data.voted_count;
	}

	private getBaseParams(message: Message): BaseSendRequest {
		const baseRequest: BaseSendRequest = {};

		if (message.isImportant()) {
			baseRequest.important = true;
		}

		if (message.isNotificationDisabled()) {
			// eslint-disable-next-line camelcase
			baseRequest.disable_notification = true;
		}

		if (message.loginRecipient() !== null) {
			baseRequest.login = message.loginRecipient()!;
		}

		if (message.chatRecipient()) {
			// eslint-disable-next-line camelcase
			baseRequest.chat_id = message.chatRecipient()!;
		}

		return baseRequest;
	}
}
