import { Message } from './message';

type OnPollResponse = () => Promise<void> | void;

type NoResponseCallback = {
	timeout: number;
	callback: OnPollResponse;
};

export class Poll extends Message {
	protected answers: string[];
	protected votedCount: number | null = null;
	protected votes = new Map<string, number>();
	protected isSubscribed = false;
	protected handlers: Map<string, OnPollResponse> = new Map();

	protected noResponseTimeouts: NoResponseCallback[] = [];
	protected timeouts = new Set<NodeJS.Timeout>();

	public constructor(text: string, answers: Record<string, string> | string[] = []) {
		super(text);
		this.answers = Array.isArray(answers) ? answers : Object.values(answers);
	}

	public getVotesCount(): number {
		return this.votedCount ?? 0;
	}

	public getAnswerVotesCount(answer: string): number {
		return this.votes.get(answer) ?? 0;
	}

	public unsubscribe(): void {
		this.clearNoResponseTimeouts();
		this.isSubscribed = false;
	}

	public onAnswer(answer: string, callback: OnPollResponse): this {
		if (!this.answers.includes(answer)) {
			this.answers.push(answer);
		}
		this.handlers.set(answer, callback);
		return this;
	}

	public onNoAnswerAfter(timeout: number, callback: OnPollResponse): this {
		this.noResponseTimeouts.push({ timeout, callback });
		return this;
	}

	protected override setId(id: number): void {
		super.setId(id);
		this.noResponseTimeouts.forEach(item => {
			// eslint-disable-next-line @typescript/no-misused-promises
			const timeout = setTimeout(item.callback, item.timeout);

			this.timeouts.add(timeout);
		});

		this.noResponseTimeouts = [];
	}

	protected receiveAnswer(answer: string): void {
		this.clearNoResponseTimeouts();
		const callback = this.handlers.get(answer);

		if (!callback) {
			return;
		}

		// eslint-disable-next-line @typescript/no-floating-promises
		callback();
	}

	private clearNoResponseTimeouts(): void {
		this.timeouts.forEach(item => clearTimeout(item));
		this.timeouts = new Set();
	}
}
