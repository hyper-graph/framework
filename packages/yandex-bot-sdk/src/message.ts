type MessageMetadata = {
	important: boolean;
	notificationDisabled: boolean;
	login: string | null;
	chatId: string | null;
};

export class Message {
	protected id: number | null = null;

	protected readonly text: string;

	protected readonly options: MessageMetadata = {
		important: false,
		notificationDisabled: false,
		login: null,
		chatId: null,
	};

	public constructor(text: string) {
		this.text = text;
	}

	public isImportant(): boolean {
		return this.options.important;
	}

	public important(): this {
		this.options.important = true;
		return this;
	}

	public isNotificationDisabled(): boolean {
		return this.options.notificationDisabled;
	}

	public disableNotification(): this {
		this.options.notificationDisabled = true;
		return this;
	}

	public isSent(): boolean {
		return this.id !== null;
	}

	public toLogin(login: string): this {
		this.options.login = login;
		return this;
	}

	public loginRecipient(): string | null {
		return this.options.login;
	}

	public toChat(chatId: string): this {
		this.options.chatId = chatId;
		return this;
	}

	public chatRecipient(): string | null {
		return this.options.chatId;
	}

	protected setId(id: number): void {
		this.id = id;
	}
}
