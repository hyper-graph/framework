import type { BaseEvent } from '../base.event';

const LISTEN_META_KEY = Symbol('LISTEN_META_KEY');
const LISTEN_METHODS_META_KEY = Symbol('LISTEN_METHODS_META_KEY');

export type EventHandler<T extends BaseEvent = BaseEvent> = (event: T) => any;

export const Listen = <T extends BaseEvent>(eventClass: Class<T, any[]>) => (
	proto: Object,
	propertyKey: string | symbol,
	_descriptor: TypedPropertyDescriptor<EventHandler<T>>,
): void => {
	const eventsToHandle = getEventsToHandle(proto, propertyKey);
	const listenMethods = getListenMethods(proto);

	eventsToHandle.push(eventClass);
	if (!listenMethods.includes(propertyKey)) {
		listenMethods.push(propertyKey);
	}

	Reflect.defineMetadata(LISTEN_META_KEY, eventsToHandle, proto, propertyKey);
	Reflect.defineMetadata(LISTEN_METHODS_META_KEY, listenMethods, proto);
};

export function getEventsToHandle(
	proto: Object,
	propertyKey: string | symbol,
): Class<BaseEvent>[] {
	return Reflect.getMetadata(LISTEN_META_KEY, proto, propertyKey) ?? [];
}

export function getListenMethods(proto: Object): (string | symbol)[] {
	return Reflect.getMetadata(LISTEN_METHODS_META_KEY, proto) ?? [];
}

export function isEventListener(proto: Object): boolean {
	return getListenMethods(proto).length > 0;
}
