import { BaseEvent } from './base.event';

export class EmptyEvent extends BaseEvent<{}> {
	public constructor() {
		super({ body: {} });
	}
}
