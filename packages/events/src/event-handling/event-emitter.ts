import type { BaseEvent } from '../base.event';
import type { BaseEventTransport } from './base.event-transport';
import type { EventPublisher } from './event-publisher';

export class EventEmitter {
	private readonly transports: BaseEventTransport[];

	public constructor(eventPublisher: EventPublisher) {
		this.transports = [eventPublisher];
	}

	public async emit(event: BaseEvent | BaseEvent[]): Promise<void> {
		await Promise.all(this.transports.map(async transport => transport.publish(event)));
	}
}
