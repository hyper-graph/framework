import 'reflect-metadata';

import {
	EventHandler,
	getEventsToHandle,
	getListenMethods,
	isEventListener,
} from '../decorators';

import type { EventPublisher } from './event-publisher';

export class EventListenerManager {
	private readonly eventPublisher: EventPublisher;

	public constructor(eventPublisher: EventPublisher) {
		this.eventPublisher = eventPublisher;
	}

	public addListener(instance: unknown): void {
		const proto = Object.getPrototypeOf(instance);
		if (!proto || !isEventListener(proto)) {
			return;
		}

		const listenMethods = getListenMethods(proto);
		const listener: Record<string | symbol, any> = instance as any;

		listenMethods.forEach(methodName => {
			const method = listener[methodName].bind(listener) as EventHandler;
			const eventsToHandle = getEventsToHandle(proto, methodName);

			eventsToHandle.forEach(eventCtor => {
				this.eventPublisher['addListener'](eventCtor, method);
			});
		});
	}
}
