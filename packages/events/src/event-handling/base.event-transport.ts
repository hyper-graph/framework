import type { BaseEvent } from '../base.event';

export abstract class BaseEventTransport {
	public abstract publish(event: BaseEvent | BaseEvent[]): Promise<void>;
}
