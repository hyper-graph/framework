import assert from 'node:assert/strict';

import { BaseEvent } from '../base.event';
import type { EventHandler } from '../decorators';
import type { BaseEventTransport } from './base.event-transport';


export class EventPublisher implements BaseEventTransport {
	private readonly listeners: Map<Class<BaseEvent>, EventHandler[]> = new Map();

	public async publish(event: BaseEvent | BaseEvent[]): Promise<void> {
		const events = Array.isArray(event) ? event : [event];

		await Promise.all(events.map(async event => this.publishOne(event)));
	}

	protected async publishOne(event: BaseEvent): Promise<void> {
		assert.ok(event instanceof BaseEvent);
		const listeners = this.findListeners(event);

		await Promise.all(listeners.map(async listener => listener(event)));
	}

	protected addListener<Event extends BaseEvent>(eventCtor: Class<Event>, listener: EventHandler<Event>): void {
		const listeners: EventHandler<Event>[] = this.listeners.get(eventCtor) ?? [];

		listeners.push(listener);

		this.listeners.set(eventCtor, listeners as EventHandler[]);
	}

	private findListeners(event: BaseEvent): EventHandler[] {
		const eventCtor = Object.getPrototypeOf(event)!.constructor;
		const listenEvents = [...this.listeners.keys()];

		return listenEvents
			.filter(ctor => eventCtor.prototype instanceof ctor || eventCtor === ctor)
			.sort((a, b) => (a.prototype instanceof b ? 1 : -1))
			.flatMap(ctor => this.listeners.get(ctor) ?? []);
	}
}
