export type EventParams<Body> = {
	body: Body;
	occurredOn?: Date;
}

export abstract class BaseEvent<Body = unknown> {
	public readonly occurredOn: Date;
	public readonly name: string;
	public readonly body: Body;

	public constructor({ body, occurredOn = new Date() }: EventParams<Body>) {
		this.body = body;
		this.occurredOn = occurredOn;
		this.name = this.constructor.name;
	}
}
