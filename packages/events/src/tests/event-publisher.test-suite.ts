// eslint-disable-next-line max-classes-per-file
import { Describe, TestSuite, Test, expect } from '@hg-ts/tests';

import { EventPublisher } from '../event-handling';

import { BaseEvent } from '../base.event';
import { EmptyEvent } from '../empty.event';

@Describe()
export class EventPublisherTestSuite extends TestSuite {
	private publisher: EventPublisher;

	@Test()
	public async publish(): Promise<void> {
		const listener = async(event: BaseEvent): Promise<void> => {
			expect(event).toBeInstanceOf(EmptyEvent);
		};

		this.publisher['addListener'](EmptyEvent, listener);

		await this.publisher.publish(new EmptyEvent());

		expect.assertions(1);
	}

	@Test()
	public async multipleListeners(): Promise<void> {
		let callIndex = 1;
		const listener1 = async(event: BaseEvent): Promise<void> => {
			expect(event).toBeInstanceOf(EmptyEvent);
			expect(callIndex).toBe(1);
			callIndex++;
		};
		const listener2 = async(event: BaseEvent): Promise<void> => {
			expect(event).toBeInstanceOf(EmptyEvent);
			expect(callIndex).toBe(2);
			callIndex++;
		};

		this.publisher['addListener'](EmptyEvent, listener1);
		this.publisher['addListener'](EmptyEvent, listener2);

		await this.publisher.publish(new EmptyEvent());

		expect.assertions(4);
	}

	@Test()
	public async multipleEvents(): Promise<void> {
		class TestEvent extends BaseEvent<{}> {
			public constructor() {
				super({ body: {} });
			}
		}
		const listener1 = async(event: BaseEvent): Promise<void> => {
			expect(event).toBeInstanceOf(EmptyEvent);
		};
		const listener2 = async(event: BaseEvent): Promise<void> => {
			expect(event).toBeInstanceOf(TestEvent);
		};

		this.publisher['addListener'](EmptyEvent, listener1);
		this.publisher['addListener'](TestEvent, listener2);

		await this.publisher.publish(new TestEvent());

		expect.assertions(1);
	}

	@Test()
	public async extendingEventsInherited(): Promise<void> {
		class TestEvent extends BaseEvent<{}> {
			public constructor() {
				super({ body: {} });
			}
		}
		class ExtendedEvent extends TestEvent {}
		const listener1 = async(event: BaseEvent): Promise<void> => {
			expect(event).toBeInstanceOf(EmptyEvent);
		};
		const listener2 = async(event: BaseEvent): Promise<void> => {
			expect(event).toBeInstanceOf(TestEvent);
		};
		const listener3 = async(event: BaseEvent): Promise<void> => {
			expect(event).toBeInstanceOf(ExtendedEvent);
		};

		this.publisher['addListener'](EmptyEvent, listener1);
		this.publisher['addListener'](TestEvent, listener2);
		this.publisher['addListener'](ExtendedEvent, listener3);

		await this.publisher.publish(new ExtendedEvent());

		expect.assertions(2);
	}

	@Test()
	public async extendingEventsParent(): Promise<void> {
		class TestEvent extends BaseEvent<{}> {
			public constructor() {
				super({ body: {} });
			}
		}
		class ExtendedEvent extends TestEvent {}
		const listener1 = async(event: BaseEvent): Promise<void> => {
			expect(event).toBeInstanceOf(EmptyEvent);
		};
		const listener2 = async(event: BaseEvent): Promise<void> => {
			expect(event).toBeInstanceOf(TestEvent);
		};
		const listener3 = async(event: BaseEvent): Promise<void> => {
			expect(event).toBeInstanceOf(ExtendedEvent);
		};

		this.publisher['addListener'](EmptyEvent, listener1);
		this.publisher['addListener'](TestEvent, listener2);
		this.publisher['addListener'](ExtendedEvent, listener3);

		await this.publisher.publish(new TestEvent());

		expect.assertions(1);
	}

	public override async beforeEach(): Promise<void> {
		this.publisher = new EventPublisher();
	}
}
