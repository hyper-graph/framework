// eslint-disable-next-line max-classes-per-file
import { Describe, TestSuite, Test, expect } from '@hg-ts/tests';
import { Listen } from '../decorators';

import { EventPublisher, EventListenerManager } from '../event-handling';

import { BaseEvent } from '../base.event';
import { EmptyEvent } from '../empty.event';

@Describe()
export class EventPublisherTestSuite extends TestSuite {
	private publisher: EventPublisher;
	private eventListenerManager: EventListenerManager;

	@Test()
	public async addListener(): Promise<void> {
		class TestEvent extends BaseEvent<{}> {
			public constructor() {
				super({ body: {} });
			}
		}
		class ExtendedEvent extends TestEvent {}
		class Listener {
			public called = new Map<Class<BaseEvent>, number>();

			@Listen(EmptyEvent)
			public async empty(event: EmptyEvent): Promise<void> {
				this.check(event, EmptyEvent);
			}

			@Listen(TestEvent)
			public async test(event: TestEvent): Promise<void> {
				this.check(event, TestEvent);
			}

			@Listen(ExtendedEvent)
			public async extended(event: ExtendedEvent): Promise<void> {
				this.check(event, ExtendedEvent);
			}

			private check(event: BaseEvent, ctor: Class<BaseEvent, any[]>): void {
				let count = this.called.get(ctor) ?? 0;

				expect(event).toBeInstanceOf(ctor);
				count++;

				this.called.set(ctor, count);
			}
		}
		const listener = new Listener();

		this.eventListenerManager.addListener(listener);

		await this.publisher.publish(new EmptyEvent());
		await this.publisher.publish(new TestEvent());
		await this.publisher.publish(new ExtendedEvent());

		expect(listener.called.get(EmptyEvent)).toBe(1);
		expect(listener.called.get(TestEvent)).toBe(2);
		expect(listener.called.get(ExtendedEvent)).toBe(1);

		expect.assertions(7);
	}
	public override async beforeEach(): Promise<void> {
		this.publisher = new EventPublisher();
		this.eventListenerManager = new EventListenerManager(this.publisher);
	}
}
