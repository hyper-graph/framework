type Properties = Record<string | symbol, unknown>;

declare global {
    // @ts-ignore
    type Nullable<T> = T | null;
    // @ts-ignore
    type NotNullable<T> = T extends null ? never : T;
    // @ts-ignore
    type Optional<T> = T | undefined;
    // @ts-ignore
    type NotOptional<T> = T extends unknown ? never : T;
    // @ts-ignore
    type Class<Instance = unknown, Argument extends unknown[] = unknown[]> = new (...args: Argument) => Instance;
    // @ts-ignore
    type AbstractClass<Instance = unknown, Argument extends unknown[] = unknown[]> = abstract new (...args: Argument) => Instance;

    interface ObjectConstructor {
        getPrototypeOf(o: unknown): Nullable<Properties & {
            constructor: Class;
        }>;
        keys<T extends Properties>(value: T): (keyof T)[];
    }
}

export {};
