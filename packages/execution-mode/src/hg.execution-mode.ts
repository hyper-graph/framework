import {
	ExecutionMode,
	ExecutionModeVariants,
} from './execution-mode';

export class HgExecutionMode extends ExecutionMode {
	public constructor() {
		super('HG', ExecutionModeVariants.PROD);
	}
}
