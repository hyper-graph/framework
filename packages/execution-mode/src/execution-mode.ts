export enum ExecutionModeVariants {
	DEBUG = 'debug',
	DEV = 'dev',
	TEST = 'test',
	QA = 'qa',
	DEMO = 'demo',
	PROD = 'prod',
}

export class ExecutionMode<ValueType extends ExecutionModeVariants = ExecutionModeVariants> {
	private value: ValueType;
	private readonly projectName: string;
	private readonly defaultValue: ValueType;

	public constructor(projectName: string, defaultValue: ValueType) {
		this.projectName = projectName.toUpperCase();
		this.defaultValue = defaultValue;
		this.initValue();
	}

	public isDebug(): boolean {
		return this.value === ExecutionModeVariants.DEBUG;
	}

	public isDev(): boolean {
		return this.value === ExecutionModeVariants.DEV;
	}

	public isTest(): boolean {
		return this.value === ExecutionModeVariants.TEST;
	}

	public isQa(): boolean {
		return this.value === ExecutionModeVariants.QA;
	}

	public isDemo(): boolean {
		return this.value === ExecutionModeVariants.DEMO;
	}

	public isProd(): boolean {
		return this.value === ExecutionModeVariants.PROD;
	}

	public getValue(): ValueType {
		return this.value;
	}

	public isValueIn(variants: ValueType[]): boolean {
		return variants.includes(this.value);
	}

	public is(expected: ValueType): boolean {
		return expected === this.value;
	}

	protected getEnvName(): string {
		return `${this.projectName}_ENV`;
	}

	protected isValueValid(value?: string): value is ValueType {
		return Object.values(ExecutionModeVariants).includes(value as any);
	}

	protected getRawValue(): string | undefined {
		return process.env[this.getEnvName()];
	}

	private initValue(): void {
		const value = this.getRawValue();

		if (this.isValueValid(value)) {
			this.value = value;
		} else {
			this.value = this.defaultValue;
		}
	}
}
