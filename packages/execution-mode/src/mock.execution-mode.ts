import {
	ExecutionMode,
	ExecutionModeVariants,
} from './execution-mode';

export class MockExecutionMode<ValueType extends ExecutionModeVariants = ExecutionModeVariants>
	extends ExecutionMode<ValueType> {
	public constructor(value: ValueType) {
		super('NEVER_USED', value);
	}
}
