import {
	ZodSchema,
	ZodTypeDef,
} from 'zod';

export type ZodDto<TOutput = any> = {
	new (): TOutput;
	isZodDto: true;
	schema: ZodSchema<TOutput, ZodTypeDef, unknown>;
	create(input: unknown): TOutput;
}

export function createZodDto<TOutput = any>(
	schema: ZodSchema<TOutput, ZodTypeDef, unknown>,
): ZodDto<TOutput> {
	class AugmentedZodDto {
		public static isZodDto = true;
		public static schema = schema;

		public static create(input: unknown): AugmentedZodDto {
			return this.schema.parse(input) as AugmentedZodDto;
		}
	}

	return AugmentedZodDto as unknown as ZodDto<TOutput>;
}
