import zod, {
	ZodEffects,
	ZodSchema,
	ZodTypeDef,
	infer as ZodInfer,
	input as ZodInput,
	ZodPipeline,
	ZodBoolean,
} from 'zod';
import {
	createZodDto,
	ZodDto,
} from './dto';

declare module 'zod' {
	// eslint-disable-next-line @typescript/consistent-type-definitions
	interface ZodSchema<Output = any, Def extends ZodTypeDef = ZodTypeDef, Input = Output> {
		enforceEnv<T extends ZodSchema<Output, Def, Input>>(this: T, name: string): ZodEffects<T, ZodInfer<T> | string>;
		transformBooleanString<T extends ZodSchema<Output, Def, Input>>(
			this: T,
		): ZodPipeline<ZodEffects<T, boolean, ZodInput<T>>, ZodBoolean>;
		toClass<T extends ZodSchema<Output, Def, Input>>(this: T): ZodDto<ZodInfer<T>>;
	}
}

zod.Schema.prototype.enforceEnv = function<T extends ZodSchema>(
	this: T,
	name: string,
): ZodEffects<T, ZodInfer<T> | string> {
	return this.transform(value => process.env[name] ?? value);
};

zod.Schema.prototype.transformBooleanString = function<T extends ZodSchema>(
	this: T,
): ZodPipeline<zod.ZodEffects<T, boolean, ZodInput<T>>, ZodBoolean> {
	return this
		.transform(value => (typeof value === 'boolean' ? value : value === 'true'))
		.pipe(zod.boolean());
};

zod.Schema.prototype.toClass = function<T extends ZodSchema>(
	this: T,
): ZodDto<ZodInfer<T>> {
	return createZodDto<ZodInfer<T>>(this);
};
