import zod from '@hg-ts/validation';

export const REDIS_CONFIG_SCHEMA = zod.object({
	host: zod.string().enforceEnv('REDIS_HOST'),
	database: zod.number().int()
		.min(0),
	port: zod.number().positive()
		.int(),
});

export class RedisConfig extends REDIS_CONFIG_SCHEMA.toClass() {}
