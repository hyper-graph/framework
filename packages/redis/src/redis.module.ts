import { ConfigLoader } from '@hg-ts/config-loader';
import {
	Global,
	Module,
} from '@hg-ts/ioc';

import {
	RedisModule as RawRedisModule,
	RedisModuleOptions,
	RedisService,
} from '@liaoliaots/nestjs-redis';

import { REDIS_CONFIG_SCHEMA } from './config';
import { RedisClient } from './redis.client';

@Global()
@Module({
	imports: [
		RawRedisModule.forRootAsync({
			async useFactory(configLoader: ConfigLoader): Promise<RedisModuleOptions> {
				const config = await configLoader.load(REDIS_CONFIG_SCHEMA, 'redis');
				return {
					commonOptions: { enableReadyCheck: true },
					config: {
						host: config.host,
						port: config.port,
						db: config.database,
					},
				};
			},
			inject: [ConfigLoader],
		} as any),
	],
	providers: [
		{
			provide: RedisClient,
			useFactory(redisService: RedisService): RedisClient {
				return redisService.getOrThrow();
			},
			inject: [RedisService],
		},
	],
	exports: [RedisClient],
})
export class RedisModule {}
