# @hg-ts/config-loader

## Examples

Example config (env DATABASE_HOST not defined by default)
If config loader not found config file (or base/first config file) it will throw exception
If config loader found multiple config files it will merge them
```ts
import { ConfigLoader, EnforceEnv } from '@hg-ts/config-loader';
import {
  IsPositive,
  IsString,
} from 'class-validator';

class DatabaseConfig {
  @IsString()
  @EnforceEnv('DATABASE_HOST')
  public host: string;

  @IsInt()
  @IsPositive()
  public port: number;
}
```

### Load config file from package root (process.cwd())

```ts
import { ConfigLoader } from '@hg-ts/config-loader';

const loader = new ConfigLoader();

// Will load `${process.cwd()}/database.json`
const config = loader.load('database', DatabaseConfig);
```

### Load config file from config directory in package root

```ts
import { ConfigLoader } from '@hg-ts/config-loader';

const loader = new ConfigLoader({ configDir: 'config' });

// Will load `${process.cwd()}/config/database.json`
const config = loader.load('database', DatabaseConfig);
```

### Load config file from package root to / recursive 

```ts
import { ConfigLoader } from '@hg-ts/config-loader';

const loader = new ConfigLoader({ recursive: true });
// process.cwd() === /opt/app/service-name

/*
 Will load
 `/opt/app/service-name/database.json`
 `/opt/app/database.json`
 `/opt/database.json` -- { root: true }
 `/database.json` -- will not be loaded cause /opt/database.json is root of this config
 */
const config = loader.load('database', DatabaseConfig);
```

### Load config file from config directory in package root with config builder

```ts
import { ConfigLoader } from '@hg-ts/config-loader';

const loader = new ConfigLoader({ configDir: 'config', envBuilder: true });

// Add config/local to .gitignore
/*
  Will load
    `${process.cwd()}/config/base/database.json`
    `${process.cwd()}/config/local/database.json`
*/
const config = loader.load('database', DatabaseConfig);
```

### Load config file from config directory in package root with config builder with env

```ts
import { ConfigLoader } from '@hg-ts/config-loader';
import { ExecutionMode, ExecutionModeVariants } from '@hg-ts/execution-mode'

const loader = new ConfigLoader({ configDir: 'config', envBuilder: true });
const env = new ExecutionMode('HG', ExecutionModeVariants.PROD)
// Add config/local to .gitignore
/*
  Will load:
    `${process.cwd()}/config/base/database.json`
    `${process.cwd()}/config/prod/database.json`
    `${process.cwd()}/config/local/database.json`
 */

/*
  Will load if process.env.HG_ENV === 'dev'
    `${process.cwd()}/config/base/database.json`
    `${process.cwd()}/config/dev/database.json`
    `${process.cwd()}/config/local/database.json`
 */

const config = loader.load('database', DatabaseConfig);
```
