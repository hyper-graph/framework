import { BaseException } from '@hg-ts/exception';

export class NoBaseConfigException extends BaseException {
	public constructor(configName: string) {
		super(`Base config with name ${configName} not found`);
	}
}
