import type { ExecutionMode } from '@hg-ts/execution-mode';
import {
	dirname,
	resolve,
} from 'path';

export type PathBuilderOptions = {

	/**
	 *
	 * Override default app path
	 * @default process.cwd()
	 */
	appPath?: string;

	/**
	 *
	 * Additional directory to appPath
	 * @default null
	 */
	configDir?: Nullable<string>;

	/**
	 *
	 * Use environment variable for build config.
	 * Get base config, apply ${env} config and, after, apply local config
	 * @default false
	 */
	envBuilder?: boolean;

	/**
	 *
	 * Override injected env
	 * Uses only with envBuilder: true
	 */
	overrideEnv?: Nullable<ExecutionMode>;

	/**
	 *
	 * Used for loading root package config. Useful with overridePostfix option
	 * @default null
	 * @todo Implement
	 */
	basePostfix?: Nullable<string>;

	/**
	 *
	 * Useful with basePostfix.
	 * For example:
	 * You can specify configName to 'hg', basePostfix to 'config' and overridePostfix to 'override'.
	 * In this case will be loaded 'bg.config.{ext}' and overrode by 'hg.override.{ext}'.
	 * Similar scheme used by many tools like 'docker-compose'.
	 * @default null
	 * @todo Implement
	 */
	overridePostfix?: Nullable<string>;

	/**
	 * Apply all configs from `appDir` to `/` until `root` field won't be specified
	 * @default false
	 * @todo Implement
	 */
	recursive?: boolean;
};

export class PathBuilder {
	public readonly options: Required<PathBuilderOptions>;
	private paths: string[];

	public constructor(options: PathBuilderOptions = {}) {
		this.options = {
			appPath: process.cwd(),
			configDir: null,
			envBuilder: false,
			overrideEnv: null,
			overridePostfix: null,
			basePostfix: null,
			recursive: false,
			...options,
		};
		this.paths = [];
	}

	public build(name: string): string[] {
		return this.prepareBasePaths()
			.addEnvFolders()
			.addConfigName(name)
			.paths;
	}

	protected prepareBasePaths(): this {
		const { appPath, configDir, recursive } = this.options;
		if (recursive) {
			let nextPath = appPath;
			const basePaths: string[] = [appPath];

			while (nextPath !== '/') {
				nextPath = dirname(nextPath);
				basePaths.push(nextPath);
			}

			this.paths = basePaths;

			if (configDir) {
				this.mergePaths([configDir]);
			}
		} else {
			this.paths = [this.buildPathToConfig([appPath, configDir])];
		}

		return this;
	}

	protected addEnvFolders(): this {
		const { envBuilder, overrideEnv } = this.options;
		const envFolders: string[] = [];

		if (!envBuilder) {
			return this;
		}

		envFolders.push('base');

		if (overrideEnv) {
			envFolders.push(overrideEnv.getValue());
		}

		envFolders.push('local');

		return this.mergePaths(envFolders);
	}

	protected addConfigName(configName: string): this {
		const { overridePostfix, basePostfix } = this.options;
		const baseName = basePostfix ? `${configName}.${basePostfix}.json` : `${configName}.json`;
		const overrideName = overridePostfix ? `${configName}.${overridePostfix}.json` : null;

		const names = [baseName];

		if (overrideName) {
			names.push(overrideName);
		}

		return this.mergePaths(names);
	}

	private mergePaths(additionalParts: string[]): this {
		this.paths = this.paths.flatMap(path => additionalParts.map(part => this.buildPathToConfig([path, part])));

		return this;
	}

	private buildPathToConfig(parts: Nullable<string>[]): string {
		const filteredParts = parts.filter((part): part is string => part !== null);

		return resolve(...filteredParts);
	}
}
