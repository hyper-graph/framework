import fs from 'node:fs/promises';
import assert from 'node:assert/strict';

import zod, { ZodSchema, ZodTypeDef } from '@hg-ts/validation';

import { NoBaseConfigException } from './exceptions';
import { PathBuilder, PathBuilderOptions } from './path-builder';

export type ConfigLoaderOptions = PathBuilderOptions & {
	recursive?: boolean;
	cache?: boolean;
};

type CacheItem = {
	schema: zod.Schema<object>;
	config: object;
};

export class ConfigLoader {
	private readonly pathBuilder: PathBuilder;
	private readonly options: ConfigLoaderOptions;
	private readonly cacheMap = new Map<string, CacheItem>();

	public constructor(options: ConfigLoaderOptions = {}) {
		this.pathBuilder = new PathBuilder(options);
		this.options = this.pathBuilder.options;
	}

	public async load<ConfigType extends object>(schema: zod.Schema<ConfigType>, name: string): Promise<ConfigType> {
		if (this.options.cache && this.cacheMap.has(name)) {
			const cacheItem = this.cacheMap.get(name)!;

			assert.ok(cacheItem.schema === schema, `cached instance of config "${name}" has another schema`);

			return cacheItem.config as ConfigType;
		}

		const rawConfig = await this.loadRawConfig(name);

		const config = await this.validate(schema, rawConfig);

		if (this.options.cache) {
			this.cacheMap.set(name, { schema, config });
		}

		return config;
	}

	private async validate<ConfigType>(
		schema: ZodSchema<ConfigType, ZodTypeDef, unknown>,
		config: unknown,
	): Promise<ConfigType> {
		return schema.parseAsync(config);
	}

	private async loadRawConfig(name: string): Promise<unknown> {
		const paths = this.pathBuilder.build(name);

		const configs = await Promise.all(paths.map(async path => this.loadConfigFile(path)));

		if (!configs[0]) {
			throw new NoBaseConfigException(name);
		}

		return this.mergeConfigs(configs);
	}

	private mergeConfigs(configs: unknown[]): Record<string, unknown> {
		return configs
			.filter(config => config !== null)
			.filter((config): config is Record<string, unknown> => typeof config === 'object')
			.reduce((merged, next) => {
				if (merged['root'] === true && this.options.recursive) {
					return merged;
				}
				return {
					...merged,
					...next,
				};
			});
	}

	private async loadConfigFile(path: string): Promise<unknown> {
		try {
			const content = await fs.readFile(path, { encoding: 'utf-8' });

			return JSON.parse(content) as unknown;
		} catch (error: unknown) {
			return null;
		}
	}
}
