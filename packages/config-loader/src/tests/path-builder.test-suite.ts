import {
	Describe,
	expect,
	Test,
	TestSuite,
} from '@hg-ts/tests';
import {
	ExecutionModeVariants,
	MockExecutionMode,
} from '@hg-ts/execution-mode';

import {
	PathBuilder,
	PathBuilderOptions,
} from '../path-builder';

@Describe()
export class PathBuilderTestSuite extends TestSuite {
	@Test()
	public async simple(): Promise<void> {
		const paths = this.getPaths();

		expect(paths).toHaveLength(1);
		expect(paths[0]).toBe('/tmp/example.json');
	}

	@Test()
	public async recursive(): Promise<void> {
		const paths = this.getPaths({ recursive: true });

		expect(paths).toHaveLength(2);
		expect(paths[0]).toBe('/tmp/example.json');
		expect(paths[1]).toBe('/example.json');
	}

	@Test()
	public async full(): Promise<void> {
		const paths = this.getPaths({
			recursive: true,
			overridePostfix: 'override',
			basePostfix: 'config',
			envBuilder: true,
			overrideEnv: new MockExecutionMode(ExecutionModeVariants.DEMO),
			configDir: 'config',
		});

		let index = 0;

		expect(paths).toHaveLength(12);
		expect(paths[index++]).toBe('/tmp/config/base/example.config.json');
		expect(paths[index++]).toBe('/tmp/config/base/example.override.json');
		expect(paths[index++]).toBe('/tmp/config/demo/example.config.json');
		expect(paths[index++]).toBe('/tmp/config/demo/example.override.json');
		expect(paths[index++]).toBe('/tmp/config/local/example.config.json');
		expect(paths[index++]).toBe('/tmp/config/local/example.override.json');

		expect(paths[index++]).toBe('/config/base/example.config.json');
		expect(paths[index++]).toBe('/config/base/example.override.json');
		expect(paths[index++]).toBe('/config/demo/example.config.json');
		expect(paths[index++]).toBe('/config/demo/example.override.json');
		expect(paths[index++]).toBe('/config/local/example.config.json');
		expect(paths[index++]).toBe('/config/local/example.override.json');
	}

	@Test()
	public async configDir(): Promise<void> {
		const paths = this.getPaths({ configDir: 'config' });

		expect(paths).toHaveLength(1);
		expect(paths[0]).toBe('/tmp/config/example.json');
	}

	@Test()
	public async envBuilder(): Promise<void> {
		const paths = this.getPaths({ envBuilder: true });

		expect(paths).toHaveLength(2);
		expect(paths[0]).toBe('/tmp/base/example.json');
		expect(paths[1]).toBe('/tmp/local/example.json');
	}

	@Test()
	public async envBuilderWithOverrideProd(): Promise<void> {
		const paths = this.getPaths({ envBuilder: true, overrideEnv: new MockExecutionMode(ExecutionModeVariants.PROD) });

		expect(paths).toHaveLength(3);
		expect(paths[0]).toBe('/tmp/base/example.json');
		expect(paths[1]).toBe('/tmp/prod/example.json');
		expect(paths[2]).toBe('/tmp/local/example.json');
	}

	@Test()
	public async envBuilderWithOverrideDev(): Promise<void> {
		const paths = this.getPaths({ envBuilder: true, overrideEnv: new MockExecutionMode(ExecutionModeVariants.DEV) });

		expect(paths).toHaveLength(3);
		expect(paths[0]).toBe('/tmp/base/example.json');
		expect(paths[1]).toBe('/tmp/dev/example.json');
		expect(paths[2]).toBe('/tmp/local/example.json');
	}

	@Test()
	public async basePostfix(): Promise<void> {
		const paths = this.getPaths({ basePostfix: 'config' });

		expect(paths).toHaveLength(1);
		expect(paths[0]).toBe('/tmp/example.config.json');
	}

	@Test()
	public async basePostfixWithOverridePostfix(): Promise<void> {
		const paths = this.getPaths({ basePostfix: 'config', overridePostfix: 'override' });

		expect(paths).toHaveLength(2);
		expect(paths[0]).toBe('/tmp/example.config.json');
		expect(paths[1]).toBe('/tmp/example.override.json');
	}

	@Test()
	public async overridePostfix(): Promise<void> {
		const paths = this.getPaths({ overridePostfix: 'override' });

		expect(paths).toHaveLength(2);
		expect(paths[0]).toBe('/tmp/example.json');
		expect(paths[1]).toBe('/tmp/example.override.json');
	}

	private getPaths(options: Omit<PathBuilderOptions, 'appPath'> = {}): string[] {
		const builder = new PathBuilder({ appPath: '/tmp', ...options });

		return builder.build('example');
	}
}
