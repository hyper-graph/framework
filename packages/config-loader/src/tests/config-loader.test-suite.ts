import zod from '@hg-ts/validation';

import {
	Describe,
	expect,
	Test,
	TestSuite,
} from '@hg-ts/tests';

import { ConfigLoader } from '../config-loader';

@Describe()
export class ConfigLoaderTestSuite extends TestSuite {
	@Test()
	public async simple(): Promise<void> {
		const loader = new ConfigLoader();

		const merged = loader['mergeConfigs']([
			{
				a: 'a',
				b: 'b',
			},
			{
				a: 'aa',
				c: 'c',
			},
		]);

		expect(merged['a']).toBe('aa');
		expect(merged['b']).toBe('b');
		expect(merged['c']).toBe('c');
	}

	@Test()
	public async recursive(): Promise<void> {
		const loader = new ConfigLoader({ recursive: true });

		const merged = loader['mergeConfigs']([
			{
				a: 'a',
				b: 'b',
			},
			{
				a: 'aa',
				c: 'c',
				root: true,
			},
			{
				a: 'aaa',
				b: 'bbb',
				c: 'ccc',
			},
		]);

		expect(merged['a']).toBe('aa');
		expect(merged['b']).toBe('b');
		expect(merged['c']).toBe('c');
	}

	@Test()
	public async enforceEnv(): Promise<void> {
		const loader = new ConfigLoader();
		const configSchema = zod.object({
			number: zod.union([zod.number(), zod.string()])
				.enforceEnv('SOME_NUMERIC_ENV')
				.transform(value => Number(value))
				.pipe(zod.number()),
			bool: zod.union([zod.string(), zod.boolean()])
				.enforceEnv('SOME_BOOLEAN_ENV')
				.transformBooleanString(),
		});

		process.env['SOME_NUMERIC_ENV'] = '10';
		process.env['SOME_BOOLEAN_ENV'] = 'false';

		const config = { number: 1, bool: true };

		const transformed = await loader['validate'](configSchema, config);

		expect(transformed.number).toBe(10);
		expect(transformed.bool).toBe(false);
	}
}
