export const INIT_HOOK = Symbol('INIT_HOOK');
export const START_HOOK = Symbol('START_HOOK');
export const DESTROY_HOOK = Symbol('DESTROY_HOOK');
export const BEFORE_SHUT_DOWN_HOOK = Symbol('BEFORE_SHUT_DOWN_HOOK');
export const SHUT_DOWN_HOOK = Symbol('SHUT_DOWN_HOOK');
