import { Module, DynamicModule } from './decorators';
import { EnvModule } from './env.module';
import { LoggerModule } from './logger.module';

@Module({ imports: [LoggerModule, EnvModule] })
export class AppModule {
	public static register(module: Class | DynamicModule): DynamicModule {
		return {
			module: AppModule,
			imports: [module],
		};
	}
}
