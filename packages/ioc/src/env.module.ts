import {
	ExecutionMode,
	HgExecutionMode,
} from '@hg-ts/execution-mode';
import {
	Global,
	Module,
} from './decorators';

@Global()
@Module({
	providers: [
		{
			provide: ExecutionMode,
			useClass: HgExecutionMode,
		},
	],
	exports: [ExecutionMode],
})
export class EnvModule {}
