import { Logger } from '@hg-ts/logger';
import {
	LoggerService,
	Logger as NestLogger,
} from '@nestjs/common';

import { Dependency } from './decorators';
import {
	INIT_HOOK,
	START_HOOK,
} from './hooks';

/* eslint-disable @typescript/explicit-module-boundary-types */
export class NestWrappedLogger implements LoggerService {
	@Dependency()
	protected readonly logger: Logger;

	public debug(message: any, context?: string): any {
		this.setOptionalContext(context);
		this.logger.debug(message);
	}

	public error(message: any, trace?: string, context?: string): any {
		this.setOptionalContext(context);
		this.logger.error(message, trace);
	}

	public log(message: any, context?: string): any {
		this.setOptionalContext(context);
		this.logger.info(message);
	}

	public verbose(message: any, context?: string): any {
		this.setOptionalContext(context);
		this.logger.notice(message);
	}

	public warn(message: any, context?: string): any {
		this.setOptionalContext(context);
		this.logger.warning(message);
	}
	/* eslint-enable @typescript/explicit-module-boundary-types */

	public async [INIT_HOOK](): Promise<void> {
		this.logger.setContext(null);
	}

	public async [START_HOOK](): Promise<void> {
		NestLogger.overrideLogger(true);
		NestLogger.overrideLogger(['debug', 'log', 'verbose', 'warn', 'error']);
		NestLogger.overrideLogger(this);
		NestLogger.flush();
	}

	private setOptionalContext(context: Nullable<string> = null): void {
		this.logger.setContext(context);
	}
}
