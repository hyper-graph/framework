import {
	Module as NestModule,
	Global as NestGlobal,
	ForwardReference,
	Inject,
	Injectable,
	InjectableOptions,
	Scope,
	forwardRef,
	Optional,
	applyDecorators,
	DynamicModule as NestDynamicModule,
} from '@nestjs/common';
import {
	INQUIRER,
	ModulesContainer,
	ModuleRef,
} from '@nestjs/core';

export type IocToken<T = unknown> = AbstractClass<T, any[]> | Class<T, any[]> | symbol | typeof INQUIRER;

type BaseProvider<T = unknown> = {
	provide: IocToken<T>;
	scope?: Scope.TRANSIENT;
};

export type FactoryDependency<T> = IocToken<T> | { token: IocToken<T>; optional: boolean };

export type ClassProvider<T> = BaseProvider<T> & {
	useClass: Class<T, any[]>;
};
export type ValueProvider<T> = BaseProvider<T> & {
	useValue: T;
};
export type FactoryProvider<T, Dependencies extends unknown[] = unknown[]> =
	BaseProvider<T> & {
	useFactory(...dependencies: Dependencies): Promise<T> | T;
	inject?: {
		[key in keyof Dependencies]: FactoryDependency<Dependencies[key]>;
	};
};
export type ExistingProvider<T = unknown> = BaseProvider<T> & {
	useExisting: IocToken<T>;
};
export type Provider<T> = Class<T> | ClassProvider<T> | ValueProvider<T> | FactoryProvider<T> | ExistingProvider<T>;
export type ModuleToken = Class | DynamicModule | NestDynamicModule | Promise<DynamicModule>;

export type ModuleMetadata<Providers extends any[] = any[]> = {
	imports?: ModuleToken[];
	providers?: {
		[key in keyof Providers]: Provider<Providers[key]>;
	};
	exports?: (ModuleToken | Provider<any> | IocToken)[];
};
export type DynamicModule = ModuleMetadata & {
	module: Class;
	global?: boolean;
}

export function Module(metadata: ModuleMetadata = {}): ClassDecorator {
	return NestModule(metadata);
}

export function Global(): ClassDecorator {
	return NestGlobal();
}

export function Dependant(options: InjectableOptions = {}): ClassDecorator {
	return Injectable(options);
}

export function Dependency(token?: IocToken): ParameterDecorator & PropertyDecorator {
	return Inject(token);
}

export function CircularDependency(token: () => IocToken): ParameterDecorator & PropertyDecorator {
	return Inject(forwardRef(token));
}

export function OptionalDependency(token?: IocToken): ParameterDecorator & PropertyDecorator {
	return applyDecorators(Inject(token), Optional()) as ParameterDecorator & PropertyDecorator;
}

export function OptionalCircularDependency(token: () => IocToken): ParameterDecorator & PropertyDecorator {
	return applyDecorators(Inject(forwardRef(token)), Optional()) as ParameterDecorator & PropertyDecorator;
}

export {
	ForwardReference,
	Scope,
	INQUIRER,
	ModulesContainer,
	ModuleRef,
};
