import { Container } from './container';
import { IocToken } from './decorators';

export class TestContainer extends Container {
	public get<T>(token: IocToken<T>): T {
		return this.application.get(token);
	}
}
