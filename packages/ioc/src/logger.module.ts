import { ExecutionMode } from '@hg-ts/execution-mode';
import {
	ConsoleLogger,
	JSONLogger,
	Logger,
} from '@hg-ts/logger';
import {
	Module,
	Global,
	Scope,
	INQUIRER,
	Provider,
} from './decorators';
import { EnvModule } from './env.module';
import { NestWrappedLogger } from './nest-wrapped.logger';

@Global()
@Module({
	imports: [EnvModule],
	providers: [
		{
			provide: Logger,
			useFactory(parent: any, executionMode: ExecutionMode): Logger {
				const context = (parent && Object.getPrototypeOf(parent)?.constructor.name) ?? null;
				const logger = executionMode.isDebug() ? new ConsoleLogger() : new JSONLogger();

				logger.setContext(context);
				return logger;
			},
			inject: [INQUIRER, ExecutionMode],
			scope: Scope.TRANSIENT,
		} satisfies Provider<Logger>,
		NestWrappedLogger,
	],
	exports: [Logger],
})
export class LoggerModule {}
