import { Logger } from '@hg-ts/logger';
import {
	Logger as NestLogger,
	ShutdownSignal,
} from '@nestjs/common';
import {
	ApplicationConfig,
	MetadataScanner,
	NestApplication,
	NestContainer,
} from '@nestjs/core';
import { AbstractHttpAdapter } from '@nestjs/core/adapters/http-adapter';
import { Injector } from '@nestjs/core/injector/injector';
import { InstanceLoader } from '@nestjs/core/injector/instance-loader';
import type { InstanceWrapper } from '@nestjs/core/injector/instance-wrapper';
import type { Module } from '@nestjs/core/injector/module';
import { NoopGraphInspector } from '@nestjs/core/inspector/noop-graph-inspector';
import { DependenciesScanner } from '@nestjs/core/scanner';
import { FastifyAdapter } from '@nestjs/platform-fastify';

import { AppModule } from './app.module';
import { DynamicModule } from './decorators';
import {
	BEFORE_SHUT_DOWN_HOOK,
	DESTROY_HOOK,
	INIT_HOOK,
	SHUT_DOWN_HOOK,
	START_HOOK,
} from './hooks';

const signals = Object.values(ShutdownSignal);

type ProviderWithHook<Hook extends symbol> = Record<Hook, () => Promise<void>>;

export class Container {
	protected application: NestApplication;
	private readonly module: DynamicModule;
	private logger: Nullable<Logger> = null;

	private initPromise: Promise<void> | null = null;
	private startPromise: Promise<void> | null = null;
	private destroyPromise: Promise<void> | null = null;
	private shutdownPromise: Promise<void> | null = null;

	public constructor(module: DynamicModule) {
		this.module = module;
	}

	public async init(): Promise<void> {
		if (!this.initPromise) {
			this.initPromise = this.internalInit();
		}
		await this.initPromise;
	}

	public async start(port: number, host = '0.0.0.0'): Promise<void> {
		if (!this.startPromise) {
			this.startPromise = this.internalStart(port, host);
		}
		await this.startPromise;
	}

	public async destroy(): Promise<void> {
		if (!this.destroyPromise) {
			this.destroyPromise = this.callHook(DESTROY_HOOK);
		}
		await this.destroyPromise;

		this.logger?.info('Application destroyed');
	}


	public async shutdown(): Promise<void> {
		if (!this.shutdownPromise) {
			this.shutdownPromise = this.internalShutdown();
		}
		await this.shutdownPromise;

		this.logger?.info('Application shutdown completed');
	}

	public static async create<T extends Container>(
		this: Class<T, [DynamicModule]>,
		module: Class | DynamicModule,
	): Promise<T> {
		const container = new this(AppModule.register(module));

		await container.internalCreate();

		return container;
	}

	private async internalInit(): Promise<void> {
		await this.application['httpAdapter'].init();
		this.application['registerParserMiddleware']();
		this.application['registerWsModule']();
		await this.application['registerRouter']();
		await this.callHook(INIT_HOOK);
		await this.application['registerRouterHooks']();
		await this.application['callBootstrapHook']();
		this.application['isInitialized'] = true;

		this.logger?.info('Application initialization completed');
	}

	private async internalStart(port: number, host: string): Promise<void> {
		await this.callHook(START_HOOK);
		this.enableShutdownHooks();

		this.logger?.info('Application bootstrapped');

		await this.application.listen(port, host);

		this.logger?.info(`Application start listening port ${port} on host ${host}`);
	}

	private async internalShutdown(): Promise<void> {
		await this.callHook(BEFORE_SHUT_DOWN_HOOK);
		await this.callHook(SHUT_DOWN_HOOK);
	}

	private enableShutdownHooks(): void {
		signals.forEach(signal => {
			// eslint-disable-next-line @typescript/no-misused-promises
			process.on(signal, this.signalHandler.bind(this));
		});
	}

	private async signalHandler(signal: NodeJS.Signals | ShutdownSignal): Promise<void> {
		try {
			signals.forEach(signal => process.removeAllListeners(signal));
			await this.destroy();
			await this.shutdown();
			process.kill(process.pid, signal);
		} catch (error: unknown) {
			// eslint-disable-next-line no-console
			console.error('ERROR_DURING_SHUTDOWN', error);
			process.exit(1);
		}
	}

	private async internalCreate(): Promise<void> {
		const httpAdapter: AbstractHttpAdapter = new FastifyAdapter();
		const container = new NestContainer();

		container.setHttpAdapter(httpAdapter);
		NestLogger.attachBuffer();
		await this.initialize(container);
		this.application = new NestApplication(
			container,
			httpAdapter,
			new ApplicationConfig(),
			NoopGraphInspector,
		);
		this.logger = await this.application.resolve<Logger>(Logger);
		this.logger.setContext(Container.name);

		this.logger.info('Application created');
	}

	private async initialize(container: NestContainer): Promise<void> {
		const injector = new Injector();
		const graphInspector = NoopGraphInspector;
		const instanceLoader = new InstanceLoader(container, injector, graphInspector);
		const metadataScanner = new MetadataScanner();
		const dependenciesScanner = new DependenciesScanner(container, metadataScanner, graphInspector);

		await dependenciesScanner.scan(this.module as any);
		await instanceLoader.createInstancesOfDependencies();
		dependenciesScanner.applyApplicationProviders();
	}

	private async callHook(hookKey: symbol): Promise<void> {
		const modules: Module[] = this.application['getModulesToTriggerHooksOn']();

		for (const module of modules) {
			await this.callModuleHooks(module, hookKey);
		}
	}

	private async callModuleHooks<T extends symbol>(module: Module, hookKey: T): Promise<void> {
		const providers = module.getNonAliasProviders();
		const [_, moduleClassHost] = providers.shift()!;
		const instances = [
			...module.controllers,
			...providers,
			...module.injectables,
		]
			.map(([_, item]) => item)
			.filter(item => item.isDependencyTreeStatic());
		const nonTransientInstances = this.getNonTransientInstances(instances);
		const transientInstances = this.getTransientInstances(instances);

		await this.callProvidersHooks(nonTransientInstances, hookKey);
		await this.callProvidersHooks(transientInstances, hookKey);
		// Call the module instance itself
		const moduleClassInstance = moduleClassHost.instance as object | null;
		if (moduleClassInstance
			&& this.hasHook(moduleClassInstance, hookKey)
			&& moduleClassHost.isDependencyTreeStatic()) {
			await moduleClassInstance[hookKey]();
		}
	}

	private getTransientInstances(providers: InstanceWrapper[]): object[] {
		return providers
			.flatMap(item => item.getStaticTransientInstances())
			// eslint-disable-next-line @typescript/no-unnecessary-condition
			.filter(item => !!item)
			.map(({ instance }) => instance);
	}

	private getNonTransientInstances(providers: InstanceWrapper[]): object[] {
		return providers
			.filter(item => !item.isTransient)
			.map(({ instance }) => instance);
	}

	private async callProvidersHooks<T extends symbol>(providers: object[], hookKey: T): Promise<void> {
		await Promise.all(
			providers
				// eslint-disable-next-line @typescript/no-unnecessary-condition
				.filter(provider => !!provider)
				.filter((provider): provider is ProviderWithHook<T> => this.hasHook(provider, hookKey))
				.map(async provider => provider[hookKey]()),
		);
	}

	private hasHook<T extends symbol>(provider: object, hookKey: T): provider is ProviderWithHook<T> {
		return typeof ((provider as any)[hookKey]) === 'function';
	}
}
