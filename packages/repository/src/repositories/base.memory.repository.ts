import type { BaseAggregate } from '@hg-ts/domain';
import { BaseRepository } from './base.repository';
import type {
	BaseFindOptions,
	AggregateId,
} from './repository';

function clone<Aggregate extends BaseAggregate<any>>(aggregate: Aggregate): Aggregate {
	const aggregatePrototype = Object.getPrototypeOf(aggregate);
	const result = Object.create(aggregatePrototype);

	Object.assign(result, aggregate);

	return result;
}

export abstract class BaseMemoryRepository<Aggregate extends BaseAggregate<any>,
    FindOptions extends BaseFindOptions<Aggregate> = BaseFindOptions<Aggregate>>
	extends BaseRepository<Aggregate, FindOptions> {
	protected lastId: Nullable<AggregateId<Aggregate>> = null;
	private readonly entitiesMap = new Map<AggregateId<Aggregate>, Aggregate>();

	public async save(aggregate: Aggregate | Aggregate[]): Promise<void> {
		if (Array.isArray(aggregate)) {
			await Promise.all(aggregate.map(async aggregate => this.save(aggregate)));
		} else {
			this.entitiesMap.set(aggregate['id'], clone(aggregate));
		}
	}

	public async delete(aggregate: Aggregate | Aggregate[]): Promise<void> {
		if (Array.isArray(aggregate)) {
			await Promise.all(aggregate.map(async aggregate => this.delete(aggregate)));
		} else {
			this.entitiesMap.delete(aggregate['id']);
		}
	}

	public async clear(): Promise<void> {
		this.entitiesMap.clear();
	}

	protected async findEntities(options: FindOptions): Promise<Aggregate[]> {
		const { id } = options;

		if (Array.isArray(id)) {
			return id.filter(id => this.entitiesMap.has(id))
				.map(id => this.entitiesMap.get(id)!);
		}

		if (typeof id !== 'undefined' && this.entitiesMap.has(id)) {
			return [this.entitiesMap.get(id)!];
		}

		return [];
	}
}
