import type { BaseAggregate } from '@hg-ts/domain';

export type AggregateId<Aggregate> = Aggregate extends BaseAggregate<infer X> ? X : never;

export type BaseFindOptions<Aggregate extends BaseAggregate<any>> = {
	id?: AggregateId<Aggregate> | AggregateId<Aggregate>[];
};

export abstract class Repository<Aggregate extends BaseAggregate<any>,
	FindOptions extends BaseFindOptions<Aggregate> = BaseFindOptions<Aggregate>> {
	public abstract getNextId(): Promise<AggregateId<Aggregate>>;

	public abstract get(id: AggregateId<Aggregate>): Promise<Nullable<Aggregate>>;

	public abstract getOrFail(id: AggregateId<Aggregate>): Promise<Aggregate>;

	public abstract find(options?: FindOptions): Promise<Aggregate[]>;

	public abstract save(entity: Aggregate | Aggregate[]): Promise<void>;

	public abstract delete(entity: Aggregate | Aggregate[]): Promise<void>;

	public abstract has(id: AggregateId<Aggregate>): Promise<boolean>;

	public abstract clear(): Promise<void>;
}
