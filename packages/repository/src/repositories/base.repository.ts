import type { BaseAggregate } from '@hg-ts/domain';
import type { AggregateNotFoundException } from '../exceptions';
import {
	BaseFindOptions,
	AggregateId,
	Repository,
} from './repository';

export const INTERNAL_SYMBOL = Symbol('Repository.Internal');

type KeyType = number | string | symbol;

export type InternalData = {
    updatedKeys: Set<KeyType>;
};

export abstract class BaseRepository<Aggregate extends BaseAggregate<any>,
    FindOptions extends BaseFindOptions<Aggregate> = BaseFindOptions<Aggregate>>
	extends Repository<Aggregate, FindOptions> {
	public async get(id: AggregateId<Aggregate>): Promise<Nullable<Aggregate>> {
		// @ts-expect-error
		const [entity] = await this.find({ id });

		return entity || null;
	}

	public async getOrFail(id: AggregateId<Aggregate>): Promise<Aggregate> {
		const entity = await this.get(id);

		if (entity === null) {
			throw this.getNotFoundException(id);
		}

		return entity;
	}

	public async has(id: AggregateId<Aggregate>): Promise<boolean> {
		const entity = await this.get(id);

		return entity !== null;
	}

	public async find(options?: Partial<FindOptions>): Promise<Aggregate[]> {
		const entities = await this.findEntities(options);

		return entities;
	}

    protected abstract findEntities(options?: Partial<FindOptions>): Promise<Aggregate[]>;

    protected abstract getNotFoundException(id: AggregateId<Aggregate>): AggregateNotFoundException;
}
