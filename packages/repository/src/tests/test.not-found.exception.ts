import { AggregateNotFoundException } from '../exceptions';
import type { TestAggregate } from './test.aggregate';

export class TestNotFoundException extends AggregateNotFoundException {
	public constructor(id: Nullable<TestAggregate['id']> = null) {
		super('Test', { id, code: 0 });
	}
}
