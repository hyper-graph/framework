import { BaseAggregate } from '@hg-ts/domain';

export class TestAggregate extends BaseAggregate {
	public someField: Nullable<string> = null;

	public constructor(id: string) {
		super({ id });
	}

	public setSomeField(value: Nullable<string>): void {
		this.someField = value;
	}
}
