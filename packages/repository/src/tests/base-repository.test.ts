import {
	Describe,
	expect,
	ExpectException,
	Test,
	TestSuite,
} from '@hg-ts/tests';
import { v4 as uuid } from 'uuid';

import { TestAggregate } from './test.aggregate';
import { TestMemoryRepository } from './test.memory.repository';
import { TestNotFoundException } from './test.not-found.exception';

@Describe()
export class BaseRepositoryTestSuite extends TestSuite {
	private readonly repository = new TestMemoryRepository();

	@Test()
	public async saveTest(): Promise<void> {
		const id = uuid();
		const aggregate = new TestAggregate(id);

		await this.repository.save(aggregate);

		const savedAggregate = await this.repository.getOrFail(id);


		expect(savedAggregate).toBeInstanceOf(TestAggregate);
		expect(savedAggregate).not.toBe(aggregate);
		expect({ ...savedAggregate }).toMatchObject({ ...aggregate });
	}

	@Test()
	@ExpectException(TestNotFoundException)
	public async getOrFailNotFoundExceptionTest(): Promise<void> {
		const id = uuid();

		await this.repository.getOrFail(id);
	}

	@Test()
	public async getNotFoundWithoutExceptionTest(): Promise<void> {
		const id = uuid();

		await this.repository.get(id);
	}

	public override async beforeEach(): Promise<void> {
		await this.repository.clear();
	}
}
