import { v4 as uuid } from 'uuid';
import type { AggregateNotFoundException } from '../exceptions';

import { BaseMemoryRepository } from '../repositories';

import type { TestAggregate } from './test.aggregate';
import { TestNotFoundException } from './test.not-found.exception';

export class TestMemoryRepository extends BaseMemoryRepository<TestAggregate> {
	public async getNextId(): Promise<TestAggregate['id']> {
		return uuid();
	}

	protected getNotFoundException(id: TestAggregate['id']): AggregateNotFoundException {
		return new TestNotFoundException(id);
	}
}
