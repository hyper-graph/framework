import { BaseException } from '@hg-ts/exception';
import { format } from 'util';

export type AggregateNotFoundParams = {
	id?: unknown;
	code?: number;
}

export abstract class AggregateNotFoundException extends BaseException {
	protected constructor(aggregateName: string, params: AggregateNotFoundParams = {}) {
		const { code, id } = params;
		const messageParts = [aggregateName];

		if (id) {
			messageParts.push(`with id ${format(id)}`);
		}

		messageParts.push('not found');

		super(messageParts.join(' '), { code });
	}
}
