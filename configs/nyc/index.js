const isCi = require('is-ci');

module.exports = {
  all: true,
  'check-coverage': !isCi,
  'skip-full': true,
  reporter: isCi ? ['none'] : ['text'],
  cache: false,
  watermarks: {
    lines: [80, 95],
    functions: [80, 95],
    branches: [80, 95],
    statements: [80, 95]
  },
  exclude: [
    'src/**/*.test.ts',
    'src/**/*.test-suite.ts',
    'src/**/*TestSuite.ts',
  ],
  branches: 80,
  lines: 80,
  functions: 80,
  statements: 80,
  'per-file': true
};
