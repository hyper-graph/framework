import {
	ChildProcess,
	spawn as nodeSpawn,
	SpawnOptions,
} from 'child_process';

export type ProcessInfo = PromiseLike<void> & {
	process: ChildProcess;
}

function getNodeOptions(env = ''): string {
	const nodeOptionParts = env.split(' ');
	const allowedIndexes: number[] = [];
	const filteredParts = nodeOptionParts.filter((item, index) => {
		switch (true) {
		case item === '--require':
		case item === '--experimental-loader':
			allowedIndexes.push(index + 1);
			return true;
		case allowedIndexes.includes(index):
			return true;
		default:
			return false;
		}
	});

	return filteredParts.join(' ');
}

export function spawn(
	command: string,
	args: string[] = [],
	options: SpawnOptions = {},
): ProcessInfo {
	const mergedEnv: Record<string, string | undefined> = {
		...process.env,
		...(options.env ?? {}),
	};
	mergedEnv['NODE_OPTIONS'] = getNodeOptions(mergedEnv['NODE_OPTIONS']);
	const childProcess = nodeSpawn(command, args, { env: mergedEnv, stdio: 'inherit', ...options });

	const promise = new Promise<void>((resolve, reject) => {
		childProcess.once('close', code => {
			if (code === 0) {
				resolve();
			} else {
				reject(new Error(`Process failed with code ${code ?? 1}`));
			}
		});
	});

	return {
		process: childProcess,
		then: promise.then.bind(promise),
	};
}
