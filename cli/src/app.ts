import yargs from 'yargs';

import {
	BuildCommand,
	BuildDevCommand,
	LintCommand,
	LintFixCommand,
	TestCommand,
	TestCoverageCommand,
	TestDevCommand,
} from './commands';

async function main(): Promise<void> {
	await yargs
		.strict()
		.command(new BuildCommand())
		.command(new BuildDevCommand())
		.command(new LintCommand())
		.command(new LintFixCommand())
		.command(new TestCommand())
		.command(new TestCoverageCommand())
		.command(new TestDevCommand())
		.parse();
}

/* eslint-disable-next-line @typescript/no-floating-promises */
main();
