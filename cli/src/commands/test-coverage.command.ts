import { TestCommand } from './test.command';

export class TestCoverageCommand extends TestCommand {
	public override readonly command: string = 'test:coverage';

	protected override getArgs(): string[] {
		return ['nyc', ...super.getArgs()];
	}
}
