import { spawn } from '../spawn';

import type { BuildOptions } from './base.command';
import {
	BuildArgs,
	BuildCommand,
} from './build.command';

type BuildDevArgs = BuildArgs & {
	onSuccess?: string;
};

export class BuildDevCommand extends BuildCommand<BuildDevArgs> {
	public override readonly command: string = 'build:dev';
	public override readonly builder: BuildOptions<BuildDevArgs> = {
		production: { default: true },
		onSuccess: {
			type: 'string',
			requiresArg: false,
		},
	};

	protected override async handle(): Promise<void> {
		await this.runTypescript('tsconfig.json');
	}

	protected override async runTypescript(configName: string): Promise<void> {
		const config = await this.getConfig(configName);

		await this.clearDist(config);

		const args = [
			'tsc-watch',
			'--project',
			configName,
			'--noClear',
		];

		if (this.args.onSuccess) {
			args.push('--onSuccess', this.args.onSuccess);
		}

		await spawn('yarn', args);
	}
}
