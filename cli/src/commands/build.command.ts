import assert from 'node:assert/strict';
import { glob } from 'glob';
import { readFile, rm } from 'node:fs/promises';
import { resolve } from 'node:path';

import { spawn } from '../spawn';

import {
	BaseCommand,
	BuildOptions,
} from './base.command';

export type BuildArgs = {
    production: boolean;
};

const FORCE_TO_PRODUCTION = process.env['NODE_ENV'] === 'production';

export class BuildCommand<Args extends BuildArgs = BuildArgs> extends BaseCommand<Args> {
	public override readonly command: string = 'build';
	public override readonly builder: BuildOptions<Args> = {
		production: {
			alias: 'p',
			boolean: true,
			default: false,
		},
	} as BuildOptions<Args>;

	protected get production(): boolean {
		return FORCE_TO_PRODUCTION || this.args.production;
	}

	protected async handle(): Promise<void> {
		const configs = await this.getConfigNames();

		await Promise.all(configs.map(async name => this.runTypescript(name)));
	}

	protected async getConfigNames(): Promise<string[]> {
		return glob('tsconfig*(\\.?*)\\.json');
	}

	protected async runTypescript(configName: string): Promise<void> {
		const config = await this.getConfig(configName);

		await this.clearDist(config);

		const args = [
			'tsc',
			'--project',
			configName,
		];

		await spawn('yarn', args);
	}

	protected async getConfig(configName: string): Promise<any> {
		const configData = await readFile(configName, 'utf8');

		return JSON.parse(configData);
	}

	// eslint-disable-next-line @typescript/explicit-module-boundary-types
	protected async clearDist(config: any): Promise<void> {
		assert.ok(config.compilerOptions?.outDir, 'outDir not specified');

		if (this.production) {
			const distPath = resolve(config.compilerOptions?.outDir);

			await rm(distPath, {
				force: true,
				recursive: true,
			});
		}
	}
}
