import isCi from 'is-ci';
import { createWriteStream } from 'node:fs';
import { resolve } from 'node:path';
import * as process from 'process';

import { spawn } from '../spawn';

import {
	BaseCommand,
	BuildOptions,
} from './base.command';

export type LintOptions = {
    fix?: boolean;
};

export class LintCommand extends BaseCommand<LintOptions> {
	public override readonly command: string = 'lint';
	public override readonly builder: BuildOptions<LintOptions> = {
		fix: {
			boolean: true,
			default: false,
		},
	};

	private readonly forceFix: boolean;

	public constructor(options: LintOptions = {}) {
		super();
		this.forceFix = options.fix ?? false;
	}

	protected get fix(): boolean {
		return this.forceFix || this.args.fix || false;
	}

	protected async handle(): Promise<void> {
		const args = [
			'eslint',
			'--cache',
			'--report-unused-disable-directives',
			'src',
		];

		if (this.fix) {
			args.push('--fix');
		}

		if (isCi) {
			args.push('--max-warnings', '0');
			args.push('--format', 'junit');
		}

		const childProcess = spawn('yarn', args, { stdio: ['inherit', isCi ? 'pipe' : 'inherit', 'inherit'] });
		const { stdout } = childProcess.process;

		if (isCi && stdout) {
			const junitOutputFile = createWriteStream(resolve('eslint.junit.xml'));
			stdout.unpipe(process.stdout);
			stdout.pipe(junitOutputFile);
			stdout.once('end', () => junitOutputFile.end());
		}

		await childProcess;
	}
}
