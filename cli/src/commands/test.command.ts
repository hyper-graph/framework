import { spawn } from '../spawn';

import {
	BaseCommand,
	BuildOptions,
} from './base.command';

export type TestOptions = {
	pattern?: string;
};

export class TestCommand extends BaseCommand<TestOptions> {
	public override readonly command: string = 'test';
	public override readonly builder: BuildOptions<TestOptions> = {
		pattern: {
			string: true,
			default: false,
		},
	};


	protected async handle(): Promise<void> {
		await spawn('yarn', this.getArgs());
	}

	protected getArgs(): string[] {
		const args = ['mocha'];

		if (this.args.pattern) {
			args.push(this.args.pattern);
		}

		return args;
	}
}
