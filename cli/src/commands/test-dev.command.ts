import { spawn } from '../spawn';

import type { BuildOptions } from './base.command';
import {
	BuildArgs,
	BuildCommand,
} from './build.command';
import type { TestOptions } from './test.command';

type TestDevArgs = BuildArgs & TestOptions;

export class TestDevCommand extends BuildCommand<TestDevArgs> {
	public override readonly command: string = 'test:dev';
	public override readonly builder: BuildOptions<TestDevArgs> = {
		production: {
			alias: 'p',
			boolean: true,
			default: false,
		},
		pattern: {
			string: true,
			default: false,
		},
	};

	protected override async handle(): Promise<void> {
		await this.runTypescript('tsconfig.json');
	}

	protected override async runTypescript(configName: string): Promise<void> {
		const config = await this.getConfig(configName);

		await this.clearDist(config);

		const args = [
			'tsc-watch',
			'--project',
			configName,
			'--noClear',
		];

		args.push('--onSuccess', 'yarn hg test');

		await spawn('yarn', args);
	}
}
