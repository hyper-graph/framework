import {
	ArgumentsCamelCase,
	CommandModule,
	Options,
} from 'yargs';

export type BuildOptions<Args> = Record<keyof Args, Options>;

export abstract class BaseCommand<Args extends Record<string, unknown>> implements CommandModule<unknown, Args> {
	protected args: Args;

	public readonly abstract command: string;
	public readonly abstract builder: BuildOptions<Args>;

	public readonly handler = async(args: ArgumentsCamelCase<Args>): Promise<void> => {
		this.args = args as unknown as Args;

		try {
			await this.handle();
		} catch {
			process.exitCode = 1;
		}
	};

    protected abstract handle(): Promise<void>;
}
