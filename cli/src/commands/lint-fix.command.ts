import { LintCommand } from './lint.command';

export class LintFixCommand extends LintCommand {
	public override readonly command: string = 'lint:fix';

	public constructor() {
		super({ fix: true });
	}
}
